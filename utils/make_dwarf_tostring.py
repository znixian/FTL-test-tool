# See lib/libelfin/dwarf/Makefile
import subprocess
from typing import List
import sys
from pathlib import Path

ROOT = Path(__file__).parent.parent


def main():
    output_file = sys.argv[1]
    with open(output_file, 'w') as fi:
        write_file(fi)


def write_file(fi):
    fi.write("// Automatically generated by make_dwarf_tostring.py\n")
    fi.write("// DO NOT EDIT\n")
    fi.write("#include <dwarf++.hh>\n")
    fi.write('#include "../elf/to_hex.hh"\n')
    fi.write("\n")
    fi.write("DWARFPP_BEGIN_NAMESPACE\n")
    run_py(fi, "dwarf++.hh", [])
    run_py(fi, "data.hh", ["-s", "_", "-u", "--hex", "-x", "hi_user", "-x", "lo_user"])
    fi.write("DWARFPP_END_NAMESPACE\n")


def run_py(fi, input_file: str, args: List[str]):
    dwarf_dir = ROOT / "lib" / "libelfin" / "dwarf"
    file = dwarf_dir / input_file
    python_file = dwarf_dir.parent / "elf" / "enum-print.py"
    with open(file, 'r') as input_fi:
        result = subprocess.run(
            [sys.executable, python_file] + args,
            stdin=input_fi,
            capture_output=True,
            encoding='utf-8',
        )
    assert result.returncode == 0
    fi.write(result.stdout)
    if result.stderr:
        print(result.stderr)


if __name__ == "__main__":
    main()
