# FTL Testing tool

This is a tool that allows you to fiddle around with FTL when you're trying
to figure stuff out. If you're trying to develop a new boarding strategy, test
some kind of timing, etc then this tool might be able to save you a lot of time.

It's effectively a large number of quick-to-use cheats: you can spawn an enemy
ship from a seed (so you can spawn the same ship repeatedly) with a simple
graphical interface, check a box to block all crew damage or prevent it from
firing a weapon, and spawn whatever crew you want onto their ship.

You can give them and yourself any selection of weapons you want, to test how
they'll behave under odd situations.

And if you make a mistake, you can use quickload to restore to a copy of the
game from a few seconds prior.

See the bottom of the readme for some screenshots.

# Download and installation

The tool is distributed as a single DLL called `hid.dll`, which is available
on [the releases page](https://gitlab.com/znixian/FTL-test-tool/-/releases).

Simply drop this DLL next to FTLGame.exe and the tool window will pop up when
you start the game. You can uninstall it by deleting the DLL. As the tool
doesn't affect the savefile system, you can install and uninstall it without
having to restart your current game.

Note you **must** use the 1.6.9 version of FTL. If you have a later copy,
the Hyperspace project provides downgrade patches for pretty much all the
storefronts you can get it from.

This tool should generally work alongside Hyperspace, so you can use both
at the same time. It's not the main focus of this mod however, so it might
have a few more bugs than when used with vanilla.

# Using the tool

Once you start or load a game, the tool window will have either two or three
tabs: overview, the player ship, and (if applicable) the enemy ship.

The console box lets you type in a command to run. The
[Hyperspace XML file](https://github.com/FTL-Hyperspace/FTL-Hyperspace/blob/a50526cd6471/Mod%20Files/data/hyperspace.xml#L275)
has a list of all the commands you can use, though a few of those (everything
after SHIP_CUSTOM) are specific to Hyperspace.

The next few buttons quickly do a few useful things:

* The 'Pirate' button loads the `PIRATE` event
* The 'Upgrade' button fully upgrades the player ship's systems and reactor
* The 'Resources' button gives you a huge amount of fuel, missiles, drones and scrap
* The 'Spawn Ship' button opens a ship-spawning window (see below).

The speed radio buttons let you adjust the speed the game is running at. This
is done by changing the time-step the game sees, so running faster than normal
might create weird bugs. The buttons command 10%, 50%, 100%, 200% and 500%
speed, for slowest/slow/normal/fast/fastest respectively.

The 'Holding (Tab) key speeds up' checkbox enables a keybind for the speed
control: holding the tab key in-game speeds it up, as though the fastest speed
option was selected. Releasing the key returns to the previously-selected speed.

The 'Block enemy weapon fire' box prevents the enemy from firing their weapons,
which is useful if you want to test boarding or something like that.

The QuickSave and QuickLoad buttons (which can also be triggered through the
F11/F12 keybinds) save and load the game. The quicksave is stored as it's own
independent savefile, so you can start a new game then quickload back to
the old one. Note that quickloading respawns the ships, so any checkboxes in
the player and enemy ship windows are reset to their default state.

The show timer checkbox lets you display an on-screen timer, in the main
FTL window. It's always paused when the game is paused, and can also be
paused while the game is running with the F9 key, or reset to zer with
the F10 key. You can move it around the screen by clicking the 'Move Timer'
button, moving your mouse to the desired location in the FTL window, and
pressing the escape key.

## The player and enemy ship windows

At the top is a list of all the crew. There's a box where you can type a room
number to send an AI-controlled crew member to a specific room.

(unfortunately, you might have to find the room numbers via trial-and-error,
though the Superluminal ship editor might show them)

This also shows the task assigned to the crew by the AI. Ignore it for
player-controlled crew.

The '...' button lets you heal or kill the crew, or switch which ship they
belong to. **Warning**: it seems that switching crew like this can interfere
with the crew AI in some way, so if you're looking for accuracy, spawn crew
on your ship and teleport them to the enemy ship. For testing boarders on
your ship though, it does seem to work fine.

The 'Crew AI Action Log' opens a window (blank until the game is unpaused)
which shows how the crew AI is assigning tasks to friendly-to-the-ship crew.
Note there's a copy of this AI for both the player and the enemy ship, with
the former being used for mind-controlled intruders and for controlling
drones, though the drone stuff isn't shown in the log. This log is shown every
frame, and can explain why the AI is assigning a given task to a given crew.

The 'Crew AI Re-assignment Log' shows all the times a crewmember is assigned
to a new task. Unlike the action log, it's not cleared every frame. It's there
to show you how crew are changing their tasks across multiple frames.

In both windows, there's a button that means the game is paused whenever the
log is changed. This lets you find out exact when the AI is changing what
tasks are being assigned, or (in the case of the action log) read it's contents
when it's changing across multiple things in quick succession.

The 'add crew' dropdown lets you add a friendly-to-the-ship crewmember of
any given race. This doesn't support custom Hyperspace races.

The 'block crew damage' checkbox disables all crew health changes, both
damage and healing. This lets you experiment with crew AI for combat without
the crew dying, or having to manually heal them.

The 'add system' button lets you spawn in a new system to the ship. Note that
enemy ships can only have certain systems added, so you can't spawn a system
that's impossible to find in that ship randomly. You can see the available
systems in Mike Hopley's fantastic [enemy ship website](https://ftl-layouts.mikehopley.org).

The 'Fix Systems' button repairs all the systems, but ignores fires and
hull breaches.

The 'Edit Systems' button opens a popup, which allows you to view and edit
the level of any system, how much power is allocated to it, and how much
damage it has. This was thrown together quickly, and might cause crashes
in some situations. Don't bother editing the power assigned to systems on
an enemy ship, as the AI will immediately change it - edit the system
level instead.

The 'Fix hull damage' checkbox repairs the ship's hull every frame.

The 'Remove ion/cooldown' removes any ion damage every frame. This is
particularly useful for systems like hacking/teleporter/etc which have
a cooldown, as that cooldown is removed instantly after they're finished.

The 'Resist weapons' box resists all incoming weapon damage, preventing
weapons from harming systems.

The 'Kill intruders' box sets the health of all intruders to zero every
frame, which is useful if you're testing an enemy ship that happens
to have a teleporter and don't want to deal with the boarders.

The 'Blueprint Palette' button opens a window that lists all the weapons,
drones, augments, and enemy ships in the game. This is used for the ship
inventory and spawn ship windows.

The 'Ship Inventory' button opens a window where you can drag items from
the aforementioned blueprint palette into the current ship, to adjust
the stuff it has. This lets you quickly spawn an enemy ship with whatever
weapons, drones and augments you want.

## Spawning ships

The 'Spawn Ship' button on the overview panel lets you spawn any enemy ship
in, with customisable parameters.

First, open the blueprint palette from the player or enemy ship tabs, and drag
in a ship to the 'ship blueprint' area.

The 'Ship is hostile' box is a little pointless, if you don't check it the ship
will be spawned but you can't attack it.

The 'Surrender at hull' and 'Escape at hull' checkboxes let you set a hull level
at which the spawned ship will attempt to surrender or escape.

The sector number and seed number are used for the ship generation: the sector
lets you set the sector for which the enemy ship is generated (earlier sectors
are of course easier, having less weapons/drones/crew/hull/systems/etc), and
the seed is a number that controls the 'random' number generator used to
generate the enemy. If you put the same value here (with the same sector and
ship blueprint), you'll always get the same ship. This lets you note down or
share a particular ship configuration you've found with others.

The seed can be any 32-bit integer, including negative values.

The spawn button doesn't close the window, so you can spawn the ship, do
something to it, then spawn it again with the same or similar settings. This
lets you quickly look for a seed with the weapons and systems you want (though
remember that you can always edit the ship to give it the exact weapons you
want, via the ship inventory window).

# Screenshots

![a screenshot of the main menu](doc/img/screenshot-overview.png)

![a screenshot of the ship menu](doc/img/screenshot-ship.png)

![a screenshot of the inventory menu](doc/img/screenshot-blueprints.png)
