//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BlueprintDialogue.rc
//
#define IDD_BLUEPRINT_SELECTOR          101
#define IDD_SHIP_BLUEPRINTS             103
#define IDD_CREW_AI_LOG                 105
#define IDD_SPAWN_SHIP                  107
#define IDD_SYSTEM_POWERS               109
#define IDD_SINGLE_SYSTEM_POWER         111
#define IDC_BLUEPRINT_LIST              1001
#define IDC_SEARCH                      1002
#define IDC_WEAPONS_LABEL               1003
#define IDC_WEAPONS_1                   1004
#define IDC_WEAPONS_2                   1005
#define IDC_WEAPONS_3                   1006
#define IDC_WEAPONS_4                   1007
#define IDC_APPLY                       1008
#define IDC_DRONES_LABEL                1009
#define IDC_DRONE_1                     1010
#define IDC_DRONE_2                     1011
#define IDC_DRONE_3                     1012
#define IDC_DRONE_4                     1013
#define IDC_AUGMENTS_LABEL              1014
#define IDC_AUGMENT_1                   1015
#define IDC_AUGMENT_2                   1016
#define IDC_AUGMENT_4                   1017
#define IDC_DND_HELP                    1018
#define IDC_CREW_ACTION_LOG             1021
#define IDC_PAUSE_ON_CHANGE             1022
#define IDC_HOSTILE                     1024
#define IDC_SURRENDER_EN                1026
#define IDC_SURRENDER_HP                1027
#define IDC_ESCAPE_EN                   1028
#define IDC_ESCAPE_HP                   1029
#define IDC_ESCAPE_TIMER                1030
#define IDC_SEED                        1031
#define IDC_SECTOR                      1032
#define IDC_SPAWN                       1033
#define IDC_SHIP_BLUEPRINT              1034
#define IDC_POWER_LIST                  1035
#define IDC_POWER_SPIN                  1037
#define IDC_POWER                       1038
#define IDC_DAMAGE_SPIN                 1039
#define IDC_DAMAGE                      1040
#define IDC_SELECTED_POWER_SPIN         1041
#define IDC_SELECTED_POWER              1042

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        113
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
