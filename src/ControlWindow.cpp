//
// Created by Campbell on 17/06/2023.
//

#include "common.h"

#include "ControlWindow.h"
#include "FTL.h"
#include "ui/BlueprintDialogue.h"
#include "ui/OverviewTab.h"
#include "ui/ShipCrewTab.h"

#include <CommCtrl.h>
#include <map>

// Link against comctl32
#pragma comment(lib, "comctl32")

// And enable visual styles
// Older versions of lld-link didn't link with this set.
#if !defined(__clang__) || __clang_major__ >= 14
#pragma comment(linker, "\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

static const wchar_t CLASS_NAME[] = L"FTT-ControlPanel";

const wchar_t *RACES[] = {L"human", L"slug", L"engi", L"mantis", L"energy", L"rock", L"crystal", L"anaerobic"};

static CApp *ftlApp = nullptr;

static void SetWindowRect(HWND hwnd, RECT rect)
{
	SetWindowPos(hwnd, nullptr, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 0);
}

ControlWindow::~ControlWindow() = default;

ControlWindow::ControlWindow()
{
	// This flag only shows the UI and doesn't start the game.
	uiTest = GetEnvironmentVariableW(L"FTL_TEST_UI_ONLY", nullptr, 0) != 0;
	isInGame = uiTest;

	instance = (HINSTANCE)GetModuleHandleW(nullptr); // breaks 16-bit support :P

	INITCOMMONCONTROLSEX initControls = {sizeof(INITCOMMONCONTROLSEX), ICC_TAB_CLASSES};
	InitCommonControlsEx(&initControls);

	// Register the window class.

	WNDCLASSEX wc = {};

	wc.lpfnWndProc = WindowProc;
	wc.hInstance = instance;
	wc.lpszClassName = CLASS_NAME;
	wc.cbWndExtra = sizeof(void *);

	wc.cbSize = sizeof(wc);                        // size of structure
	wc.style = CS_HREDRAW | CS_VREDRAW;            // redraw if size changes
	wc.cbClsExtra = 0;                             // no extra class memory
	wc.hIcon = nullptr;                            // Use the default icon
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);   // predefined arrow
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); // Default window background colour
	wc.lpszMenuName = nullptr;                     // No default window
	wc.hIconSm = nullptr;

	RegisterClassEx(&wc);

	// Create the window.

	mainWindow = CreateWindowEx(
		0,                   // Optional window styles.
		CLASS_NAME,          // Window class
		L"FTL Test Tool",    // Window text
		WS_OVERLAPPEDWINDOW, // Window style

		// Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

		nullptr,      // Parent window
		nullptr,      // Menu
		wc.hInstance, // Instance handle
		nullptr       // Additional application data
	);
	if (mainWindow == nullptr)
	{
		showError("Failed to create control window");
	}
	SetWindowLongPtr(mainWindow, 0, (LONG)this);

	// Create a not-horribly-ugly font to use for everything
	generalFont = CreateFont(
		16, 0, 0, 0, FW_NORMAL, false, false, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		CLEARTYPE_QUALITY, FF_DONTCARE | VARIABLE_PITCH, nullptr
	);

	// Add in it's contents
	PopulateWindow();

	ShowWindow(mainWindow, SW_SHOWDEFAULT);

	UpdateLayout();

	if (uiTest)
	{
		// Run the message loop. It's convenient for testing.
		MSG msg = {};
		while (GetMessage(&msg, nullptr, 0, 0) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		ExitProcess(0);
	}
}

void ControlWindow::PopulateWindow()
{
	tabControl = CreateWindow(
		WC_TABCONTROL,         // Predefined class; Unicode assumed
		L"SectionsTab",        // Control name
		WS_VISIBLE | WS_CHILD, // Styles
		0, 0, 0, 0,            // Position is set later
		mainWindow,            // Parent window
		nullptr,               // No menu.
		instance, nullptr
	);
	SendMessage(tabControl, WM_SETFONT, (WPARAM)generalFont, true);

	UpdateTabItems();
}

void ControlWindow::UpdateTabItems()
{
	TabCtrl_DeleteAllItems(tabControl);

	TCITEM tab;
	tab.mask = TCIF_TEXT | TCIF_IMAGE;
	tab.iImage = -1;
	tab.pszText = wcsdup(L"Overview");
	TabCtrl_InsertItem(tabControl, 0, &tab);

	if (isInGame)
	{
		tab.pszText = wcsdup(L"Player ship");
		TabCtrl_InsertItem(tabControl, 1, &tab);
	}

	if (isInGame && enemyShipManager != nullptr)
	{
		tab.pszText = wcsdup(L"Enemy ship");
		TabCtrl_InsertItem(tabControl, 2, &tab);
	}

	// Select the overview tab by default
	TabCtrl_SetCurSel(tabControl, 0);

	UpdateTabContents();
}

void ControlWindow::UpdateTabContents()
{
	if (currentTab != nullptr)
	{
		DestroyWindow(currentTab);
		currentTab = nullptr;
	}

	if (!isInGame)
	{
		playerShip.reset();
	}

	currentTab = CreateWindow(
		WC_STATIC,             // Predefined class; Unicode assumed
		L"",                   // Displayed on the edge of the control
		WS_VISIBLE | WS_CHILD, // Styles
		0, 0, 0, 0,            // Position is set later
		mainWindow,            // Parent window
		nullptr,               // No menu.
		instance, nullptr
	);
	baseTabWFN = (WNDPROC)SetWindowLongPtr(currentTab, GWL_WNDPROC, (LONG)WindowProcTab);

	int window = TabCtrl_GetCurSel(tabControl);
	if (window == 0)
	{
		PopulateOverview();
	}
	else if (window == 1)
	{
		PopulateTabAI(false, playerShip);
	}
	else if (window == 2)
	{
		PopulateTabAI(true, enemyShip);
	}

	UpdateLayout();
}

void ControlWindow::OpenBlueprintDialogue()
{
	std::shared_ptr<BlueprintDialogue> dialogue = std::make_unique<BlueprintDialogue>(mainWindow);
	AbstractDialogue::allDialogues.push_back(dialogue);
	dialogue->strongRef = dialogue;
}

void ControlWindow::PopulateOverview()
{
	if (!overview || overview->isInGame != isInGame)
	{
		overview = std::make_unique<OverviewTab>(this, ftlApp);
	}

	overview->AddControls(currentTab);
}

void ControlWindow::PopulateTabAI(bool isEnemyShip, std::unique_ptr<ShipCrewTab> &tab)
{
	ShipManager *ship;
	if (uiTest)
		ship = nullptr;
	else if (isEnemyShip)
		ship = enemyShipManager;
	else
		ship = ftlApp->world->playerShip->shipManager;

	if (!tab)
	{
		tab = std::make_unique<ShipCrewTab>(this, ship, ftlApp);
	}

	tab->AddControls(currentTab);
}

void ControlWindow::UpdateLayout()
{
	RECT rect;
	GetClientRect(mainWindow, &rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	SetWindowPos(tabControl, nullptr, 0, 0, width, height, 0);

	GetClientRect(tabControl, &rect);
	TabCtrl_AdjustRect(tabControl, false, &rect);
	SetWindowRect(currentTab, rect);
}

HWND ControlWindow::CreateLabel(const std::wstring &label, int x, int y)
{
	HDC dc = GetDC(mainWindow);
	SelectObject(dc, generalFont);
	SIZE size;
	GetTextExtentPoint32(dc, label.c_str(), (int)label.size(), &size);

	HWND hwnd = CreateWindow(
		WC_STATIC,              // Predefined class
		label.c_str(),          // Label
		WS_VISIBLE | WS_CHILD,  // Styles
		x, y, size.cx, size.cy, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)generalFont, true);

	return hwnd;
}

LRESULT CALLBACK ControlWindow::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	ControlWindow *window = (ControlWindow *)GetWindowLongPtr(hwnd, 0);

	// This happens early, before we call SetWindowLongPtr.
	if (window == nullptr)
	{
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	return window->WindowProcImpl(hwnd, uMsg, wParam, lParam);
}

LRESULT ControlWindow::WindowProcImpl(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	int window = TabCtrl_GetCurSel(tabControl);
	if (window == 0 && overview)
	{
		LRESULT result;
		if (overview->ParentWindowProc(hwnd, uMsg, wParam, lParam, &result))
		{
			return result;
		}
	}

	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hwnd, &ps);

		// All painting occurs here, between BeginPaint and EndPaint.

		FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));

		EndPaint(hwnd, &ps);
		return 0;
	}
	case WM_SIZE: {
		UpdateLayout();
		break; // Also use default handling
	}
	case WM_SETCURSOR: {
		// Fight with FTL, which hides it.
		// First hide it, then show it again. This keeps the counter reasonable.
		int value = ShowCursor(false);
		while (value < 0)
		{
			value = ShowCursor(true);
		}
		break; // Let the default resizing etc cursor work
	}
	case WM_NOTIFY: {
		// Sent by a control
		NMHDR *hdr = (NMHDR *)lParam;
		if (hdr->hwndFrom == tabControl && hdr->code == TCN_SELCHANGE)
		{
			UpdateTabContents();
			return 0;
		}
		break;
	}
	default:
		break; // Do nothing.
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT ControlWindow::WindowProcTab(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HWND root = GetAncestor(hwnd, GA_ROOT);
	ControlWindow *window = (ControlWindow *)GetWindowLongPtr(root, 0);
	return window->WindowProcImplTab(hwnd, uMsg, wParam, lParam);
}
LRESULT ControlWindow::WindowProcImplTab(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT result;

	int window = TabCtrl_GetCurSel(tabControl);
	if (window == 0 && overview && overview->WindowProc(hwnd, uMsg, wParam, lParam, &result))
	{
		return result;
	}
	if (window == 1 && playerShip && playerShip->WindowProc(hwnd, uMsg, wParam, lParam, &result))
	{
		return result;
	}
	if (window == 2 && enemyShip && enemyShip->WindowProc(hwnd, uMsg, wParam, lParam, &result))
	{
		return result;
	}

	return CallWindowProc(baseTabWFN, hwnd, uMsg, wParam, lParam);
}

void ControlWindow::Update(CApp *app)
{
	ftlApp = app;

	// If we've switched into our out of a game, update accordingly.
	bool newInGame = !app->menu.bOpen; // Is the main menu open?
	ShipManager *newEnemyShip;

	if (!newInGame)
	{
		newEnemyShip = nullptr;
	}
	else
	{
		newEnemyShip = nullptr;
		for (CompleteShip *ship : app->world->ships)
		{
			if (ship != app->world->playerShip)
				newEnemyShip = ship->shipManager;
		}
	}

	if (newInGame != isInGame || newEnemyShip != enemyShipManager)
	{
		isInGame = newInGame;
		enemyShipManager = newEnemyShip;
		UpdateTabItems();
	}

	if (!isInGame)
	{
		return;
	}

	// Some things are supposed to happen to the ship, whether or not
	// the tab is currently selected.
	if (playerShip)
	{
		bool valid = playerShip->UpdateShip(ftlApp->world->playerShip->shipManager);
		if (!valid)
		{
			playerShip.reset();
			UpdateTabItems();
			return;
		}
	}
	if (enemyShip)
	{
		bool valid = enemyShip->UpdateShip(enemyShipManager);
		if (!valid)
		{
			enemyShip.reset();
			UpdateTabItems();
			return;
		}
	}

	int window = TabCtrl_GetCurSel(tabControl);

	if (window == 0 && overview)
	{
		overview->UpdateUI();
	}
	if (window == 1 && playerShip)
	{
		playerShip->UpdateUI();
	}
	if (window == 2 && enemyShip)
	{
		enemyShip->UpdateUI();
	}
}

void ControlWindow::UpdateIntruders(CrewAI *ai)
{
}

int ControlWindow::GetPriorityOverride(CrewAI *ai, CrewMember *crew)
{
	if (playerShip && playerShip->OwnsCrew(crew))
	{
		return playerShip->GetPriorityOverride(ai, crew);
	}
	if (enemyShip && enemyShip->OwnsCrew(crew))
	{
		return enemyShip->GetPriorityOverride(ai, crew);
	}

	return -1;
}

bool ControlWindow::BlockDamage(CrewMember *crew)
{
	if (playerShip && playerShip->OwnsCrew(crew))
	{
		return playerShip->BlockCrewDamage(crew);
	}
	if (enemyShip && enemyShip->OwnsCrew(crew))
	{
		return enemyShip->BlockCrewDamage(crew);
	}

	return false;
}

bool ControlWindow::BlockAIWeapons()
{
	return overview && overview->BlockAIWeapons();
}

bool ControlWindow::ResistDamage(ShipManager *ship)
{
	if (playerShip && playerShip->OwnsShip(ship))
	{
		return playerShip->ResistShipDamage();
	}
	if (enemyShip && enemyShip->OwnsShip(ship))
	{
		return enemyShip->ResistShipDamage();
	}

	return false;
}

void ControlWindow::HandleMessages()
{
	HandleMessagesFor(mainWindow);

	for (int i = (int)AbstractDialogue::allDialogues.size() - 1; i >= 0; i--)
	{
		const auto iter = AbstractDialogue::allDialogues.begin() + i;
		std::shared_ptr<AbstractDialogue> dialogue = iter->lock();
		if (!dialogue)
		{
			// Dialogue was closed, remove it from the list
			AbstractDialogue::allDialogues.erase(iter);
			continue;
		}

		HandleMessagesFor(dialogue->GetWindowHandle());
	}
}

void ControlWindow::HandleMessagesFor(HWND window)
{
	if (window == nullptr)
		return;

	// Poll new messages.
	MSG msg = {};
	while (PeekMessageA(&msg, window, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

bool ControlWindow::TryHandleWindow(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
	if (hwnd != mainWindow)
		return false;

	*result = WindowProcImpl(hwnd, uMsg, wParam, lParam);
	return true;
}

void ControlWindow::ShowPopupMenu(HMENU menu, HWND parent)
{
	POINT cursor = {};
	GetCursorPos(&cursor);
	TrackPopupMenuEx(
		menu,
		TPM_LEFTALIGN | TPM_TOPALIGN | TPM_LEFTBUTTON | TPM_NOANIMATION, // Styles
		cursor.x, cursor.y,                                              // The position to open the menu at
		parent, nullptr
	);
}

std::wstring ControlWindow::GetText(HWND hwnd)
{
	std::wstring currentText;
	currentText.resize(GetWindowTextLengthW(hwnd) + 1);
	GetWindowTextW(hwnd, currentText.data(), (int)currentText.size());
	currentText.erase(currentText.size() - 1); // Remove the trailing null
	return currentText;
}

void ControlWindow::SetText(HWND hwnd, const std::wstring &string)
{
	// Avoid flickering when we change a label every frame
	if (GetText(hwnd) == string)
		return;

	SetWindowTextW(hwnd, string.c_str());
}

void ControlWindow::PushCrewAIMessage(CrewAI *ai, CrewActionLog &&message)
{
	if (playerShip && playerShip->OwnsShip(ai->ship))
	{
		playerShip->PushCrewAIMessage(std::move(message));
	}
	else if (enemyShip && enemyShip->OwnsShip(ai->ship))
	{
		enemyShip->PushCrewAIMessage(std::move(message));
	}
}

void ControlWindow::PushCrewReassignedMessage(CrewAI *ai, CrewReassignmentLog &&message)
{
	if (playerShip && playerShip->OwnsShip(ai->ship))
	{
		playerShip->PushCrewReassignedMessage(std::move(message));
	}
	else if (enemyShip && enemyShip->OwnsShip(ai->ship))
	{
		enemyShip->PushCrewReassignedMessage(std::move(message));
	}
}

float ControlWindow::GetGameSpeed(bool tabPressed)
{
	return overview->GetGameSpeed(tabPressed);
}
