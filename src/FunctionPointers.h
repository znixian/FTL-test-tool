//
// Created by Campbell on 17/06/2023.
//

#pragma once

#include <map>
#include <string>

class FunctionPointers
{
  public:
	static void Init();
	static FunctionPointers &Instance();

	FunctionPointers(const FunctionPointers &other) = delete;
	FunctionPointers &operator=(const FunctionPointers &other) = delete;

	// Required for smart-pointers.
	~FunctionPointers();

	class FPImpl;

	void *LookupSymbol(const std::string &sym) const;

  private:
	FunctionPointers();

	std::map<std::string, void *> symbolTable;
};
