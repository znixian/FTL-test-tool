//
// Created by Campbell on 17/06/2023.
//

#pragma once

// Include stuff required for the big FTL header
#include <stdint.h>

// From Hooks.h, don't include it just for this
void MemFreeFTL(void *memory);
void *MemNewFTL(int size);

// Include a few basic RapidXML types that the header needs
namespace rapidxml
{

template <typename T> struct xml_node
{
};

} // namespace rapidxml

// Make up a little implementation of libstdc++'s members
namespace ftl_std
{
struct string
{
	char *data = nullptr;
	int length = 0;
	char stuff[16] = {0};

	void wrapExternalStorage(std::string &external)
	{
		data = external.data();
		length = (int)external.size();
	}

	void wrapLiteral(const char *literal)
	{
		data = (char *)literal;
		length = (int)strlen(literal);
	}

	void allocateAndCopy(const std::string &other)
	{
		deleteStoredString();

		// Null-terminate the strings for c_str()
		int dataSize = (int)other.size() + 1;
		data = (char *)MemNewFTL(dataSize);
		ZeroMemory(data, dataSize);
		length = (int)other.size();

		memcpy(data, other.c_str(), other.size());
	}

	void deleteStoredString()
	{
		// Short-string storage?
		if (data == stuff || data == nullptr)
			return;

		MemFreeFTL(data);
		data = nullptr;
		length = 0;
	}

	[[nodiscard]] std::wstring toWide() const
	{
		return ConvertNarrowString(std::string(data, length));
	}
};
static_assert(sizeof(string) == 24);

template <typename A, typename B> struct pair
{
	A first;
	B second;
};

enum tree_colour : int
{
	TC_RED = 0,
	TC_BLACK,
};

struct map_base_node
{
	tree_colour colour;
	map_base_node *parent;
	map_base_node *left;
	map_base_node *right;
};

template <typename K, typename V> struct map
{
	struct entry
	{
		K key;
		V value;
	};

	struct value_node : public map_base_node
	{
		entry entry;

		const value_node *get_right() const
		{
			return (const value_node *)right;
		}

		const value_node *get_left() const
		{
			return (const value_node *)left;
		}

		const value_node *get_parent() const
		{
			return (const value_node *)parent;
		}
	};

	struct iterator
	{
		const value_node *node;

		void operator++()
		{
			// See GCC's implementation for the basics:
			// https://github.com/gcc-mirror/gcc/blob/4832767db789/libstdc%2B%2B-v3/src/c%2B%2B98/tree.cc#L60
			// We're starting at the tree's left-most node, and move upwards
			// until we find a node we haven't fully visited (which occurs
			// when we're on the right-hand side of that node).
			// If we're currently on a node with a right-hand child, visit
			// that - we know that we've already visited it's left-hand child,
			// if it has one.

			// Check if we have a right-hand subtree to visit.
			if (node->right != nullptr)
			{
				node = node->get_right();

				// Start at the left-most node of that subtree, and work our way up.
				while (node->left)
					node = node->get_left();

				return;
			}

			// We've finished visiting some sub-tree, walk upwards to find
			// out how large that subtree is. If we've visited a subtree, it means
			// that our current node must be it's rightmost node.
			// This means we're walking up the right-hand side of the tree, so we'll
			// stop as soon as we're on the left-hand side of some node.
			while (node->parent->right == node)
			{
				node = node->get_parent();
			}

			// We're now on the left-hand side of a node we've never visited before.
			// Now switch over to visit it, and subsequently visit it's whole sub-tree.
			// If we were on the last node, we've iterated up into the root node, which
			// means by taking it's parent we've gone into the header node, which is
			// fine since that's what end() returns and it means we should stop iterating.
			// There's one edge-case we have to guard against, which is a tree with
			// only one node. In that case, because the header node's right-hand child is
			// also the root node, we're now currently sitting on the header node.
			// For a better explanation, see https://stackoverflow.com/a/73671259.
			bool isHeaderOfSingleNodeTree = node->right == node->parent;
			if (!isHeaderOfSingleNodeTree)
				node = node->get_parent();
		}

		const entry &operator*() const
		{
			return ((const value_node *)node)->entry;
		}

		const entry *operator->() const
		{
			return &((const value_node *)node)->entry;
		}

		bool operator==(iterator other) const
		{
			return other.node == node;
		}
	};

	[[nodiscard]] iterator begin() const
	{
		return iterator{(const value_node *)header.left};
	}

	[[nodiscard]] iterator end() const
	{
		return iterator{(const value_node *)&header};
	}

	char allocator[4];
	map_base_node header;
	int size;
};
static_assert(sizeof(map_base_node) == 16);
static_assert(sizeof(map<int, int>) == 24);

template <typename K, typename V> struct unordered_map
{
	char stuff[28];
};
static_assert(sizeof(unordered_map<int, int>) == 28);

template <typename T> struct vector
{
	T *beginPtr;
	T *endPtr;
	T *allocEnd;

	struct iterator
	{
		using value_type = T; // Required for std::range support

		T *ptr;

		iterator operator++(int) // iter++ with the ignored dummy parameter NOLINT(cert-dcl21-cpp)
		{
			iterator prev = *this;
			ptr++;
			return prev;
		}

		iterator &operator++() // ++iter
		{
			ptr++;
			return *this;
		}

		bool operator==(const iterator &other) const
		{
			return ptr == other.ptr;
		}

		const T &operator->() const
		{
			return ptr;
		}

		operator const T *() const // NOLINT(google-explicit-constructor)
		{
			return ptr;
		}

		const T &operator*() const
		{
			return *ptr;
		}
	};

	[[nodiscard]] int indexOf(const T &item) const
	{
		for (T *iter = beginPtr; iter < endPtr; iter++)
		{
			if (item == *iter)
				return iter - beginPtr;
		}
		return -1;
	}

	[[nodiscard]] int size() const
	{
		return endPtr - beginPtr;
	}

	const T &operator[](int id) const
	{
		return beginPtr[id];
	};

	[[nodiscard]] iterator begin() const
	{
		return iterator{beginPtr};
	}

	[[nodiscard]] iterator end() const
	{
		return iterator{endPtr};
	}
};

// Boolean vectors are special
template <> struct vector<bool>
{
	char stuff[20];
};

static_assert(sizeof(vector<int>) == 12);
static_assert(sizeof(vector<bool>) == 20);
static_assert(std::input_or_output_iterator<vector<int>::iterator>);
static_assert(std::ranges::input_range<vector<int>>);

template <typename T> struct queue
{
	char stuff[40];
};
static_assert(sizeof(queue<int>) == 40);

} // namespace ftl_std

enum SystemID
{
	SYS_SHIELDS = 0,
	SYS_ENGINES = 1,
	SYS_OXYGEN = 2,
	SYS_WEAPONS = 3,
	SYS_DRONE = 4,
	SYS_MEDBAY = 5,
	SYS_PILOT = 6,
	SYS_SENSORS = 7,
	SYS_DOORS = 8,
	SYS_TELEPORTER = 9,
	SYS_CLOAKING = 10,
	SYS_ARTILLERY = 11,
	SYS_BATTERY = 12,

	// Advanced Edition
	SYS_CLONEBAY = 13,
	SYS_MIND_CONTROL = 14,
	SYS_HACKING = 15,

	SYS_COUNT = 16,

	SYS_REACTOR_SPECIAL = 17, // This is... odd
};
extern const wchar_t *SYSTEM_NAMES[SYS_COUNT];

#define LIBZHL_PLACEHOLDER ;

#define std ftl_std
#include "../ftl-structs/FTLGameWin32.h"
#undef std
