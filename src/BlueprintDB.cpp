//
// Created by Campbell on 19/06/2023.
//

#include "common.h"

#include "BlueprintDB.h"

#include "FTL.h"
#include "Hooks.h"
#include "ThiscallHook.h"

const wchar_t *BLUEPRINT_TYPE_NAMES[] = {L"Weapon", L"Drone", L"Augment", L"Ship"};

BlueprintDB::~BlueprintDB() = default;

BlueprintDB &BlueprintDB::GetInstance()
{
	static BlueprintDB instance;
	return instance;
}

BlueprintDB::BlueprintDB()
{
	if (TheBlueprintManager->weaponBlueprints.size == 0)
	{
		// UI test mode.

		auto addBlueprint = [&](const wchar_t *id, const wchar_t *name, BlueprintType type) {
			blueprints.push_back(BlueprintInfo{nullptr, id, name, type});
		};

		addBlueprint(L"ID_TEST_1", L"Test Blueprint 1", BP_TYPE_WEAPON);
		addBlueprint(L"ID_TEST_2", L"Test Blueprint 2", BP_TYPE_DRONE);
		addBlueprint(L"ID_TEST_3", L"Test Blueprint 3", BP_TYPE_AUGMENT);
		addBlueprint(L"ID_TEST_4", L"Test Blueprint 4", BP_TYPE_SHIP);
	}
	else
	{
		for (const auto &[ftlId, constBlueprint] : TheBlueprintManager->weaponBlueprints)
		{
			AddBlueprint((Blueprint *)&constBlueprint, BP_TYPE_WEAPON);
		}
		for (const auto &[ftlId, constBlueprint] : TheBlueprintManager->droneBlueprints)
		{
			AddBlueprint((Blueprint *)&constBlueprint, BP_TYPE_DRONE);
		}
		for (const auto &[ftlId, constBlueprint] : TheBlueprintManager->augmentBlueprints)
		{
			AddBlueprint((Blueprint *)&constBlueprint, BP_TYPE_AUGMENT);
		}
		for (const auto &[ftlId, blueprint] : TheBlueprintManager->shipBlueprints)
		{
			// Ship blueprints use their own ID/name fields.
			// Note we use the ship class rather than it's name, as that's
			// what is displayed in the combat UI.
			blueprints.push_back(BlueprintInfo{
				const_cast<ShipBlueprint *>(&blueprint),
				blueprint.blueprintName.toWide(),
				base_GetTextString(&blueprint.shipClass),
				BlueprintType::BP_TYPE_SHIP,
			});
		}
	}

	for (BlueprintInfo &bp : blueprints)
	{
		byName[bp.id] = &bp;
	}
}

void BlueprintDB::AddBlueprint(Blueprint *blueprint, BlueprintType type)
{
	// We can't just call blueprint->GetNameLong() due to the calling conventions
	// in MSVC and GCC being different :(
	void **vtable = *(void ***)blueprint;
	void *fnGetNameLong = vtable[3];

	ftl_std::string ftlName;
	DWORD args[] = {(DWORD)blueprint};
	thishook::callBasegame(fnGetNameLong, &ftlName, 1, args);

	std::wstring name = ftlName.toWide();
	ftlName.deleteStoredString();

	blueprints.push_back(BlueprintInfo{
		blueprint,
		blueprint->name.toWide(),
		name,
		type,
	});
}

BlueprintDB::iterator BlueprintDB::begin()
{
	iterator iter;
	iter.idx = 0;
	return iter;
}
BlueprintDB::iterator BlueprintDB::end()
{
	iterator iter;
	iter.idx = (int)blueprints.size();
	return iter;
}
BlueprintInfo *BlueprintDB::Lookup(const std::wstring &id)
{
	auto iter = byName.find(id);
	if (iter == byName.end())
		return nullptr;

	return iter->second;
}

BlueprintInfo *BlueprintDB::Lookup(const Blueprint *ftlBlueprint)
{
	if (ftlBlueprint == nullptr)
		return nullptr;

	return Lookup(ftlBlueprint->name.toWide());
}

void BlueprintDB::iterator::operator++()
{
	idx++;
}
bool BlueprintDB::iterator::operator==(BlueprintDB::iterator other)
{
	return idx == other.idx;
}

BlueprintInfo *BlueprintDB::iterator::operator*()
{
	return &BlueprintDB::GetInstance().blueprints.at(idx);
}
