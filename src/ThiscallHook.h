//
// Created by Campbell on 17/06/2023.
//

#pragma once

// This MUST be used on thiscall hook functions!
#define THISCALL_HOOK __cdecl

struct FastCallContext;

namespace thishook
{

void addHook(void *baseFunc, void *overrideFunc, int numArgs);
DWORD callOriginal(FastCallContext *context, void *thisPtr);
DWORD callOriginal(FastCallContext *context, void *thisPtr, DWORD *args);
DWORD callBasegame(void *funcPtr, void *thisPtr, int numArgs, void *args);

}; // namespace thishook
