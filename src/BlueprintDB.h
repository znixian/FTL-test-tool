//
// Created by Campbell on 19/06/2023.
//

#pragma once

#include <unordered_map>
#include <vector>

extern const wchar_t *BLUEPRINT_TYPE_NAMES[];
enum BlueprintType
{
	BP_TYPE_WEAPON,
	BP_TYPE_DRONE,
	BP_TYPE_AUGMENT,
	BP_TYPE_SHIP,
};

struct BlueprintInfo
{
	/// Note this is null in the UI test mode!
	Blueprint *blueprint = nullptr;

	std::wstring id;
	std::wstring name;

	BlueprintType type = BP_TYPE_WEAPON;
};

class BlueprintDB
{
  public:
	~BlueprintDB();

	BlueprintDB(const BlueprintDB &other) = delete;
	BlueprintDB &operator=(const BlueprintDB &other) = delete;

	static BlueprintDB &GetInstance();

	class iterator
	{
	  public:
		void operator++();
		bool operator==(iterator other);
		BlueprintInfo *operator*();

	  private:
		int idx = 0;

		friend BlueprintDB;
	};

	iterator begin();
	iterator end();

	BlueprintInfo *Lookup(const Blueprint *ftlBlueprint);
	BlueprintInfo *Lookup(const std::wstring &id);

  private:
	BlueprintDB();

	void AddBlueprint(Blueprint *blueprint, BlueprintType type);

	std::vector<BlueprintInfo> blueprints;
	std::unordered_map<std::wstring, BlueprintInfo *> byName;
};
