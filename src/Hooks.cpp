//
// Created by Campbell on 17/06/2023.
//

#include "common.h"

#include "Hooks.h"

#include "ControlWindow.h"
#include "FTL.h"
#include "FunctionPointers.h"
#include "OnScreenTimer.h"
#include "ThiscallHook.h"
#include "ui/CrewActionDialogue.h"
#include "ui/CrewReassignmentDialogue.h"

#include <codecvt>
#include <locale>
#include <memory>
#include <ole2.h>
#include <subhook.h>

// Stop clang-format from splitting this!
#define DECLSPEC_NAKED __declspec(naked)

using subhook::Hook;
using subhook::ScopedHookRemove;

Hook *sh_sil_main = nullptr;
Hook *sh_input_update = nullptr;
Hook *sh_window_proc = nullptr;
Hook *sh_RegisterClassExU = nullptr;
Hook *sh_FileHelper_getSaveFile = nullptr;
Hook *sh_FileHelper_deleteFile = nullptr;

std::unique_ptr<ControlWindow> window;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
static std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
#pragma clang diagnostic pop

static std::wstring overrideSaveFileName;
static bool blockFileDeletion = false;
static int overrideSectorNumber = -1;
static CApp *ftlApp = nullptr;
static bool tabPressed = false;

int(__cdecl *sil_main)(int argc, char **argv);
void(__stdcall *input_update)();
LRESULT(CALLBACK *window_proc)(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
ATOM(__stdcall *RegisterClassExU)(void *lpwcx);
void(__cdecl *ftl_free)(void *memory); // In case we're using a different C runtime
void *(__cdecl *ftl_new)(int size);    // In case we're using a different C runtime
ftl_std::string *(__cdecl *FileHelper_getSaveFile)(ftl_std::string *out);
void(__cdecl *FileHelper_deleteFile)(ftl_std::string *file);
ftl_std::string *(__cdecl *FileHelper_getUserFolder)(ftl_std::string *out);
int(__cdecl *ShipSystem_NameToSystemId)(ftl_std::string *name);
void(__cdecl *DrawTextWithGlow)(int x, int y, bool centred, ftl_std::string *text, GL_Color colour, float alpha);
PowerManager *(__cdecl *PowerManager_GetPowerManager)(int shipId);

// Thiscall functions:
void *App_OnLoop;
void *App_OnKeyUp;
void *App_OnKeyDown;
void *CommandGui_RunCommand;
void *CommandGui_OnCleanup;
void *CFPS_OnLoop;
void *CFPS_OnRender;
void *TextString_GetText;
void *CrewAI_UpdateIntruders;
void *CrewAI_ChoosePriorityRoom;
void *CrewAI_OnLoop;
void *CrewAI_UpdateCrewMember;
void *CrewAI_AttemptToAssign;
void *CrewAI_PrioritizeTask;
void *CombatAI_UpdateWeapons;
void *Equipment_CheckContents;
void *ShipManager_AddCrewMember;
void *ShipManager_AddSystem;
void *ShipManager_ResistDamage;
void *ShipManager_SetTarget;
void *ShipManager_AddWeapon;
void *ShipManager_RemoveWeapon;
void *ShipManager_AddDrone;
void *ShipManager_RemoveDrone;
void *ShipObject_AddAugmentation;
void *ShipObject_RemoveAugmentation;
void *ShipGenerator_CreateShip;
void *SpaceManager_AddShip;
void *CrewMember_DirectModifyHealth;
void *CrewMember_GetName;
void *CrewMember_SetTask;
void *CrewMember_MoveToRoom;
void *WorldManager_SaveGame;
void *WorldManager_LoadGame;
void *WorldManager_OnCleanup;
void *WorldManager_CreateShip;
void *WorldManager_ClearLocation;

// Global objects
CFPS *FPSControl = nullptr;
MouseControl *TheGameMouse = nullptr;
BlueprintManager *TheBlueprintManager = nullptr;
ftl_std::vector<ShipInfo> *ShipInfoList = nullptr;

OnScreenTimer GameTimer;
bool HasHyperspace = false;

static void InstallSecondaryHooks();

int __cdecl hook_sil_main(int argc, char **argv)
{
	// We need OLE for blueprint drag-and-drop
	OleInitialize(nullptr);

	HasHyperspace = GetModuleHandleW(L"Hyperspace.dll") != nullptr;

	// If present, Hyperspace should have done it's signature search.
	// Wait until now to avoid breaking it with our hook instructions.
	InstallSecondaryHooks();

	window = std::make_unique<ControlWindow>();

	ScopedHookRemove shr(sh_sil_main);
	return sil_main(argc, argv);
}

void __stdcall hook_input_update()
{
	// Can't call this in App::OnLoop since it doesn't run when the window isn't focused.
	// Note this isn't made redundant by hook_window_proc because that
	// runs on a separate thread, and thus won't receive the bulk of our messages.
	window->HandleMessages();

	ScopedHookRemove shr(sh_input_update);
	input_update();
}

LRESULT CALLBACK hook_window_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// Let our window process stuff first, to avoid the cursor being hidden.
	LRESULT result;
	bool handled = window->TryHandleWindow(hwnd, uMsg, wParam, lParam, &result);
	if (handled)
	{
		return result;
	}

	ScopedHookRemove shr(sh_window_proc);
	return window_proc(hwnd, uMsg, wParam, lParam);
}

ATOM __stdcall hook_RegisterClassExU(void *lpwcx)
{
	ScopedHookRemove shr(sh_RegisterClassExU);
	return RegisterClassExU(lpwcx);
}

ftl_std::string *__cdecl hook_FileHelper_getSaveFile(ftl_std::string *out)
{
	if (!overrideSaveFileName.empty())
	{
		out->allocateAndCopy(converter.to_bytes(overrideSaveFileName));
		return out;
	}

	ScopedHookRemove shr(sh_FileHelper_getSaveFile);
	return FileHelper_getSaveFile(out);
}

void __cdecl hook_FileHelper_deleteFile(ftl_std::string *file)
{
	if (blockFileDeletion)
	{
		return;
	}

	ScopedHookRemove shr(sh_FileHelper_deleteFile);
	FileHelper_deleteFile(file);
}

void THISCALL_HOOK hook_App_OnLoop(FastCallContext *context, CApp *app)
{
	ftlApp = app;
	thishook::callOriginal(context, app);

	window->Update(app);

	bool paused = app->gui->bPaused || app->gui->menu_pause;
	if (!paused && !app->menu.bOpen)
	{
		GameTimer.OnLoop();
	}
}

void THISCALL_HOOK hook_App_OnKeyUp(FastCallContext *context, CApp *app, SDLKey key)
{
	if (key == SDLK_TAB)
	{
		tabPressed = false;
		return;
	}

	thishook::callOriginal(context, app);
}

void THISCALL_HOOK hook_App_OnKeyDown(FastCallContext *context, CApp *app, SDLKey key)
{
	if (key == SDLK_F11)
	{
		QuickSave();
		return;
	}
	if (key == SDLK_F12)
	{
		QuickLoad();
		return;
	}

	if (key == SDLK_ESCAPE && GameTimer.moving && GameTimer.visible)
	{
		GameTimer.moving = false;
		return;
	}
	if (key == SDLK_F10 && GameTimer.visible)
	{
		GameTimer.seconds = 0.f;
		return;
	}
	if (key == SDLK_F9 && GameTimer.visible)
	{
		GameTimer.running = !GameTimer.running;
		return;
	}

	if (key == SDLK_TAB)
	{
		tabPressed = true;
		return;
	}

	thishook::callOriginal(context, app);
}

void THISCALL_HOOK hook_CFPS_OnLoop(FastCallContext *context, CFPS *fps)
{
	thishook::callOriginal(context, fps);

	float multiplier = window->GetGameSpeed(tabPressed);

	// Do this rather than modifying speedLevel as we have finer control
	fps->SpeedFactor *= multiplier;
}

void THISCALL_HOOK hook_CFPS_OnRender(FastCallContext *context, CFPS *fps)
{
	// Note that CFPS.OnRender is the last thing that gets run before
	// the rendering stuff is finished.
	thishook::callOriginal(context, fps);

	// Don't show the timer in the menu
	if (!ftlApp->menu.bOpen && GameTimer.visible)
	{
		GameTimer.OnRender();
	}
}

void THISCALL_HOOK hook_CrewAI_UpdateIntruders(FastCallContext *context, CrewAI *ai)
{
	thishook::callOriginal(context, ai);

	window->UpdateIntruders(ai);

	window->PushCrewAIMessage(ai, CrewActionLog{nullptr, L"Updating intruders"});
}

int THISCALL_HOOK hook_CrewAI_ChoosePriorityRoom(FastCallContext *context, CrewAI *ai, CrewMember *crew)
{
	// This is only used for intruders

	int override = window->GetPriorityOverride(ai, crew);
	if (override != -1)
	{
		window->PushCrewAIMessage(ai, CrewActionLog{crew, L"Overriding priority room: " + std::to_wstring(override)});
		return override;
	}

	int result = (int)thishook::callOriginal(context, ai);
	window->PushCrewAIMessage(ai, CrewActionLog{crew, L"Priority room: " + std::to_wstring(result)});
	return result;
}

void THISCALL_HOOK hook_CrewAI_OnLoop(FastCallContext *context, CrewAI *ai)
{
	window->PushCrewAIMessage(ai, {nullptr, L"Start update, initial state:"});
	for (CrewMember *crew : ai->ship->vCrewList)
	{
		window->PushCrewAIMessage(ai, {crew, L"| " + CrewTaskToString(ai, crew, crew->task), 1});
	}

	if (ai->bOverrideRace)
	{
		window->PushCrewAIMessage(ai, {nullptr, L"Override engi/zoltan multiracial crew."});
	}

	thishook::callOriginal(context, ai);

	window->PushCrewAIMessage(ai, {nullptr, L"Tasks this update:"});
	for (const CrewTask &task : ai->desiredTaskList)
	{
		window->PushCrewAIMessage(ai, {nullptr, L"| " + CrewTaskToString(ai, nullptr, task), 1});
	}

	// Make sure we send any crew whose room we've overridden to the correct
	// room. By overriding CrewMember::MoveToRoom we make them always go
	// to the correct room if they're sent anywhere, but if they're stationary
	// they won't start moving on their own.
	// That override is still needed, to avoid the crew trying to walk out of
	// their target room.
	for (CrewMember *crew : ai->ship->vCrewList)
	{
		int override = window->GetPriorityOverride(ai, crew);
		if (override == -1 || crew->iRoomId == override)
			continue;

		// Intruders are controlled separately, via ChoosePriorityRoom
		// Args are: int roomId, int slotId, bool forceMove
		// Use roomId=0 and let the MoveToRoom hook change it, to make
		// sure it's not broken as it might be harder to test.
		DWORD args[] = {(DWORD)0, (DWORD)-1, (DWORD) false};
		thishook::callBasegame(CrewMember_MoveToRoom, crew, 3, args);
	}
}

void THISCALL_HOOK hook_CrewAI_UpdateCrewMember(FastCallContext *context, CrewAI *ai, int crewId)
{
	thishook::callOriginal(context, ai);

	CrewMember *crew = ai->crewList[crewId];

	window->PushCrewAIMessage(ai, {crew, L"Initial update: " + CrewTaskToString(ai, crew, crew->task)});
}

bool THISCALL_HOOK hook_CrewAI_AttemptToAssign(FastCallContext *context, CrewAI *ai, CrewTask task)
{
	window->PushCrewAIMessage(ai, {nullptr, L"Attempting to assign task: " + CrewTaskToString(ai, nullptr, task)});

	return (bool)thishook::callOriginal(context, ai);
}

// For some reason, it looks like using CrewTask in the method was passing
// it in as a pointer, which doesn't match FTL's ABI.
int THISCALL_HOOK
hook_CrewAI_PrioritizeTask(FastCallContext *context, CrewAI *ai, int taskId, int taskRoom, int taskSystem, int crewId)
{
	int original = (int)thishook::callOriginal(context, ai);
	return original;
}

void THISCALL_HOOK hook_CombatAI_UpdateWeapons(FastCallContext *context, CombatAI *ai)
{
	if (window->BlockAIWeapons())
		return;

	thishook::callOriginal(context, ai);
}

bool THISCALL_HOOK hook_ShipManager_ResistDamage(FastCallContext *context, ShipManager *ship, ftl_std::string &aug)
{
	if (window->ResistDamage(ship))
		return true;

	return (bool)thishook::callOriginal(context, ship);
}

ShipManager *THISCALL_HOOK hook_ShipGenerator_CreateShip(
	FastCallContext *context, ShipGenerator *gen, ftl_std::string *name, int sector, ShipEvent *event
)
{
	if (overrideSectorNumber != -1)
	{
		sector = overrideSectorNumber;
	}

	DWORD args[] = {(DWORD)name, (DWORD)sector, (DWORD)event};
	return (ShipManager *)thishook::callOriginal(context, gen, args);
}

bool THISCALL_HOOK hook_CrewMember_DirectModifyHealth(FastCallContext *context, CrewMember *crew, float amount)
{
	if (window->BlockDamage(crew))
		return false;

	return (bool)thishook::callOriginal(context, crew);
}

void THISCALL_HOOK hook_CrewMember_SetTask(FastCallContext *context, CrewMember *crew, CrewTask task)
{
	CrewTask oldTask = crew->task;

	thishook::callOriginal(context, crew);

	CompleteShip *ship = GetCompleteShip(crew->ship);
	if (ship == nullptr)
		return;

	CrewAI *ai = &ship->shipAI.crewAI;
	window->PushCrewAIMessage(ai, {crew, L"Set task: " + CrewTaskToString(ai, crew, task)});

	// Log changes in tasks to the separate dialogue
	if (task.taskId != oldTask.taskId || task.room != oldTask.room || task.system != oldTask.system)
	{
		int index = ship->shipManager->vCrewList.indexOf(crew);
		// In the form "name (id,race)"
		std::wstring crewIdent =
			base_GetCrewName(crew) + L" (" + std::to_wstring(index) + L"," + crew->species.toWide() + L")";
		CrewReassignmentLog log = {
			crewIdent,
			CrewTaskToString(ai, crew, oldTask),
			CrewTaskToString(ai, crew, task),
		};
		window->PushCrewReassignedMessage(ai, std::move(log));
	}
}

bool THISCALL_HOOK
hook_CrewMember_MoveToRoom(FastCallContext *context, CrewMember *crew, int roomId, int slotId, bool forceMove)
{
	// If the user has set a movement override, apply that.
	CompleteShip *ship = GetCompleteShip(crew->ship);
	int overrideRoom = window->GetPriorityOverride(&ship->shipAI.crewAI, crew);
	if (overrideRoom != -1 && roomId != overrideRoom)
	{
		if (crew->iRoomId == overrideRoom)
		{
			return true; // TODO value
		}
		DWORD args[] = {(DWORD)overrideRoom, (DWORD)-1, (DWORD)forceMove};
		return (bool)thishook::callOriginal(context, crew, args);
	}

	return (bool)thishook::callOriginal(context, crew);
}

CompleteShip *THISCALL_HOOK
hook_WorldManager_CreateShip(FastCallContext *context, WorldManager *world, ShipEvent *ship, bool boss)
{
	return (CompleteShip *)thishook::callOriginal(context, world);
}

void InstallHooks()
{
	FunctionPointers &fp = FunctionPointers::Instance();

#define LOAD_FN(var, name) var = (decltype(var))fp.LookupSymbol(name)
	LOAD_FN(sil_main, "_sil_main");
	LOAD_FN(input_update, "_input_update");
	LOAD_FN(window_proc, "_window_proc@16");
	LOAD_FN(RegisterClassExU, "_RegisterClassExU");
	LOAD_FN(ftl_free, "__ZdlPv");
	LOAD_FN(ftl_new, "__Znwj");
	LOAD_FN(FileHelper_getSaveFile, "__ZN10FileHelper11getSaveFileB5cxx11Ev");
	LOAD_FN(
		FileHelper_deleteFile, "__ZN10FileHelper10deleteFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(FileHelper_getUserFolder, "__ZN10FileHelper13getUserFolderB5cxx11Ev");
	LOAD_FN(
		ShipSystem_NameToSystemId,
		"__ZN10ShipSystem14NameToSystemIdENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(
		DrawTextWithGlow,
		"__ZL16DrawTextWithGlowRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE5Pointb8GL_Colorf"
	);
	LOAD_FN(PowerManager_GetPowerManager, "__ZN12PowerManager15GetPowerManagerEi");

	LOAD_FN(App_OnLoop, "__ZN4CApp6OnLoopEv");
	LOAD_FN(App_OnKeyUp, "__ZN4CApp7OnKeyUpEi");
	LOAD_FN(App_OnKeyDown, "__ZN4CApp9OnKeyDownEi");
	LOAD_FN(
		CommandGui_RunCommand, "__ZN10CommandGui10RunCommandERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(CommandGui_OnCleanup, "__ZN10CommandGui9OnCleanupEv");
	LOAD_FN(CFPS_OnLoop, "__ZN4CFPS6OnLoopEv");
	LOAD_FN(CFPS_OnRender, "__ZN4CFPS8OnRenderEv");
	LOAD_FN(TextString_GetText, "__ZNK10TextString7GetTextB5cxx11Ev");
	LOAD_FN(CrewAI_UpdateIntruders, "__ZN6CrewAI15UpdateIntrudersEv");
	LOAD_FN(CrewAI_ChoosePriorityRoom, "__ZN6CrewAI18ChoosePriorityRoomEP10CrewMember");
	LOAD_FN(CrewAI_OnLoop, "__ZN6CrewAI6OnLoopEv");
	LOAD_FN(CrewAI_UpdateCrewMember, "__ZN6CrewAI16UpdateCrewMemberEi");
	LOAD_FN(CrewAI_AttemptToAssign, "__ZN6CrewAI15AttemptToAssignE8CrewTask");
	LOAD_FN(CrewAI_PrioritizeTask, "__ZN6CrewAI14PrioritizeTaskE8CrewTaski");
	LOAD_FN(CombatAI_UpdateWeapons, "__ZN8CombatAI13UpdateWeaponsEv");
	LOAD_FN(Equipment_CheckContents, "__ZN9Equipment13CheckContentsEv");
	LOAD_FN(
		ShipManager_AddCrewMember,
		"__ZN11ShipManager13AddCrewMemberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_bibb"
	);
	LOAD_FN(ShipManager_AddSystem, "__ZN11ShipManager9AddSystemEi");
	LOAD_FN(
		ShipManager_ResistDamage, "__ZN11ShipManager12ResistDamageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(ShipManager_SetTarget, "__ZN11ShipManager9SetTargetEPS_");
	LOAD_FN(ShipManager_AddWeapon, "__ZN11ShipManager9AddWeaponEPK15WeaponBlueprinti");
	LOAD_FN(ShipManager_RemoveWeapon, "__ZN11ShipManager12RemoveWeaponEi");
	LOAD_FN(ShipManager_AddDrone, "__ZN11ShipManager8AddDroneEPK14DroneBlueprinti");
	LOAD_FN(ShipManager_RemoveDrone, "__ZN11ShipManager11RemoveDroneEi");
	LOAD_FN(
		ShipObject_AddAugmentation,
		"__ZN10ShipObject15AddAugmentationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(
		ShipObject_RemoveAugmentation,
		"__ZN10ShipObject18RemoveAugmentationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"
	);
	LOAD_FN(
		ShipGenerator_CreateShip,
		"__ZN13ShipGenerator10CreateShipENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi9ShipEvent"
	);
	LOAD_FN(SpaceManager_AddShip, "__ZN12SpaceManager7AddShipEP11ShipManager");
	LOAD_FN(CrewMember_DirectModifyHealth, "__ZN10CrewMember18DirectModifyHealthEf");
	LOAD_FN(CrewMember_GetName, "__ZN10CrewMember7GetNameB5cxx11Ev");
	LOAD_FN(CrewMember_SetTask, "__ZN10CrewMember7SetTaskE8CrewTask");
	LOAD_FN(CrewMember_MoveToRoom, "__ZN10CrewMember10MoveToRoomEiib");
	LOAD_FN(WorldManager_SaveGame, "__ZN12WorldManager8SaveGameEv");
	LOAD_FN(WorldManager_LoadGame, "__ZN12WorldManager8LoadGameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE");
	LOAD_FN(WorldManager_OnCleanup, "__ZN12WorldManager9OnCleanupEv");
	LOAD_FN(WorldManager_CreateShip, "__ZN12WorldManager10CreateShipE9ShipEventb");
	LOAD_FN(WorldManager_ClearLocation, "__ZN12WorldManager13ClearLocationEv");

	LOAD_FN(FPSControl, "__ZN4CFPS10FPSControlE");
	LOAD_FN(TheGameMouse, "__ZN12MouseControl5MouseE");
	LOAD_FN(TheBlueprintManager, "__ZN16BlueprintManager10BlueprintsE");
	LOAD_FN(ShipInfoList, "__ZN10ShipObject12shipInfoListE");
#undef LOAD_FN

	// This hook installs all the other hooks
	sh_sil_main = new Hook((void *)sil_main, (void *)hook_sil_main);
	sh_sil_main->Install();
}

static void InstallSecondaryHooks()
{
	// This is run later on, once Hyperspace (if present) has installed it's hooks.

	sh_input_update = new Hook((void *)input_update, (void *)hook_input_update);
	sh_input_update->Install();

	sh_window_proc = new Hook((void *)window_proc, (void *)hook_window_proc);
	sh_window_proc->Install();

	sh_RegisterClassExU = new Hook((void *)RegisterClassExU, (void *)hook_RegisterClassExU);
	sh_RegisterClassExU->Install();

	sh_FileHelper_getSaveFile = new Hook((void *)FileHelper_getSaveFile, (void *)hook_FileHelper_getSaveFile);
	sh_FileHelper_getSaveFile->Install();

	sh_FileHelper_deleteFile = new Hook((void *)FileHelper_deleteFile, (void *)hook_FileHelper_deleteFile);
	sh_FileHelper_deleteFile->Install();

	thishook::addHook(App_OnLoop, (void *)hook_App_OnLoop, 0);
	thishook::addHook(App_OnKeyUp, (void *)hook_App_OnKeyUp, 1);
	thishook::addHook(App_OnKeyDown, (void *)hook_App_OnKeyDown, 1);
	thishook::addHook(CFPS_OnLoop, (void *)hook_CFPS_OnLoop, 0);
	thishook::addHook(CFPS_OnRender, (void *)hook_CFPS_OnRender, 0);
	thishook::addHook(CrewAI_UpdateIntruders, (void *)hook_CrewAI_UpdateIntruders, 0);
	thishook::addHook(CrewAI_ChoosePriorityRoom, (void *)hook_CrewAI_ChoosePriorityRoom, 1);
	thishook::addHook(CrewAI_OnLoop, (void *)hook_CrewAI_OnLoop, 0);
	thishook::addHook(CrewAI_UpdateCrewMember, (void *)hook_CrewAI_UpdateCrewMember, 1);
	thishook::addHook(CrewAI_AttemptToAssign, (void *)hook_CrewAI_AttemptToAssign, 3);   // From CrewTask size
	thishook::addHook(CrewAI_PrioritizeTask, (void *)hook_CrewAI_PrioritizeTask, 3 + 1); // 3 from CrewTask size
	thishook::addHook(CombatAI_UpdateWeapons, (void *)hook_CombatAI_UpdateWeapons, 0);
	thishook::addHook(ShipManager_ResistDamage, (void *)hook_ShipManager_ResistDamage, 1);
	thishook::addHook(ShipGenerator_CreateShip, (void *)hook_ShipGenerator_CreateShip, 3);
	thishook::addHook(CrewMember_DirectModifyHealth, (void *)hook_CrewMember_DirectModifyHealth, 1);
	thishook::addHook(CrewMember_SetTask, (void *)hook_CrewMember_SetTask, 3); // From CrewTask size
	thishook::addHook(CrewMember_MoveToRoom, (void *)hook_CrewMember_MoveToRoom, 3);
	thishook::addHook(WorldManager_CreateShip, (void *)hook_WorldManager_CreateShip, 2);
}

CrewMember *base_AddCrewMember(
	ShipManager *ship, const std::wstring &name, const std::wstring &type, bool intruder, int roomId, bool init,
	bool male
)
{
	std::string narrowName = converter.to_bytes(name);
	std::string narrowType = converter.to_bytes(type);

	ftl_std::string ftlName;
	ftl_std::string ftlType;
	ftlName.wrapExternalStorage(narrowName);
	ftlType.wrapExternalStorage(narrowType);

	DWORD args[] = {(DWORD)&ftlName, (DWORD)&ftlType, intruder, (DWORD)roomId, init, male};
	return (CrewMember *)thishook::callBasegame(ShipManager_AddCrewMember, ship, 6, args);
}

void base_AddSystem(ShipManager *ship, int systemId)
{
	// Note the function returns an int, but I don't know (and don't care
	// enough to find out) what it is.

	DWORD args[] = {(DWORD)systemId};
	thishook::callBasegame(ShipManager_AddSystem, ship, 1, args);
}

void base_RunCommand(CommandGui *gui, const std::wstring &command)
{
	std::string narrowCommand = converter.to_bytes(command);

	ftl_std::string ftlCommand;
	ftlCommand.wrapExternalStorage(narrowCommand);

	DWORD args[] = {(DWORD)&ftlCommand};
	thishook::callBasegame(CommandGui_RunCommand, gui, 1, args);
}

std::wstring base_GetCrewName(CrewMember *crew)
{
	ftl_std::string ftlString;

	// Returning a large struct puts it in ECX, and this in stack+4
	DWORD args[] = {(DWORD)crew};
	thishook::callBasegame(CrewMember_GetName, &ftlString, 1, args);

	std::wstring result = converter.from_bytes(ftlString.data, ftlString.data + ftlString.length);
	ftlString.deleteStoredString();

	return result;
}

std::wstring base_GetTextString(const TextString *string)
{
	ftl_std::string result;

	// Note the 'this' and return args are swapped due to
	// the GCC calling convention.
	DWORD args[] = {(DWORD)string};
	thishook::callBasegame(TextString_GetText, &result, 1, args);

	std::wstring wide = result.toWide();
	result.deleteStoredString();

	return wide;
}

void base_CheckEquipmentContents(Equipment *equipment)
{
	thishook::callBasegame(Equipment_CheckContents, equipment, 0, nullptr);
}

void base_SetShipTarget(ShipManager *ship, ShipManager *target)
{
	DWORD args[] = {(DWORD)target};
	thishook::callBasegame(ShipManager_SetTarget, ship, 1, args);
}

void base_AddWeapon(ShipManager *ship, WeaponBlueprint *blueprint, int slot)
{
	DWORD args[] = {(DWORD)blueprint, (DWORD)slot};
	thishook::callBasegame(ShipManager_AddWeapon, ship, 2, args);
}

void base_RemoveWeapon(ShipManager *ship, int slot)
{
	DWORD args[] = {(DWORD)slot};
	thishook::callBasegame(ShipManager_RemoveWeapon, ship, 1, args);
}

void base_AddDrone(ShipManager *ship, DroneBlueprint *blueprint, int slot)
{
	DWORD args[] = {(DWORD)blueprint, (DWORD)slot};
	thishook::callBasegame(ShipManager_AddDrone, ship, 2, args);
}

void base_RemoveDrone(ShipManager *ship, int slot)
{
	DWORD args[] = {(DWORD)slot};
	thishook::callBasegame(ShipManager_RemoveDrone, ship, 1, args);
}

void base_AddAugmentation(ShipManager *ship, const std::wstring &id)
{
	// Note that ShipManager inherits from ShipObject, so this is safe.
	std::string narrowId = converter.to_bytes(id);
	ftl_std::string ftlId;
	ftlId.wrapExternalStorage(narrowId);
	DWORD args[] = {(DWORD)&ftlId};
	thishook::callBasegame(ShipObject_AddAugmentation, ship, 1, args);
}

void base_RemoveAugmentation(ShipManager *ship, const std::wstring &id)
{
	// Note that ShipManager inherits from ShipObject, so this is safe.
	std::string narrowId = converter.to_bytes(id);
	ftl_std::string ftlId;
	ftlId.wrapExternalStorage(narrowId);
	DWORD args[] = {(DWORD)&ftlId};
	thishook::callBasegame(ShipObject_RemoveAugmentation, ship, 1, args);
}

void MemFreeFTL(void *memory)
{
	ftl_free(memory);
}

void *MemNewFTL(int size)
{
	return ftl_new(size);
}

void SaveGame(const std::wstring &saveFilePath)
{
	overrideSaveFileName = saveFilePath;
	thishook::callBasegame(WorldManager_SaveGame, ftlApp->world, 0, nullptr);
	overrideSaveFileName.clear();
}

void LoadGame(const std::wstring &saveFilePath)
{
	std::string narrowPath = converter.to_bytes(saveFilePath);

	ftl_std::string ftlPath;
	ftlPath.wrapExternalStorage(narrowPath);

	// Clean up the GUI first - this also happens when you return to the menu
	// or hangar, and if you don't do this then the GUI will continue pointing
	// to player's ShipManager after it's deleted, which can cause a crash
	// in Equipment::SetPosition when it tries to load stuff from the old ship.
	thishook::callBasegame(CommandGui_OnCleanup, ftlApp->gui, 0, nullptr);

	// Open windows don't like being loaded over
	for (FocusWindow *win : ftlApp->gui->focusWindows)
	{
		win->bOpen = false;
	}

	// Don't delete our custom save file
	blockFileDeletion = true;

	DWORD args[] = {(DWORD)&ftlPath};
	thishook::callBasegame(WorldManager_LoadGame, ftlApp->world, 1, args);

	blockFileDeletion = false;
}

static std::wstring GetSaveDir()
{
	// Use FTL's code to ensure we get the same directory.
	ftl_std::string ftlString;
	FileHelper_getUserFolder(&ftlString);

	std::wstring result = converter.from_bytes(ftlString.data);
	ftlString.deleteStoredString();
	return result;
}

static std::wstring GetQuickSavePath()
{
	return GetSaveDir() + L"test-tool-quicksave.sav";
}

void QuickSave()
{
	// Block in the main menu
	if (ftlApp->menu.bOpen)
		return;

	SaveGame(GetQuickSavePath());
}

void QuickLoad()
{
	// Block in the main menu
	if (ftlApp->menu.bOpen)
		return;

	LoadGame(GetQuickSavePath());
}

int NameToSystemId(const std::wstring &systemName)
{
	std::string narrowName = converter.to_bytes(systemName);
	ftl_std::string ftlName;
	ftlName.wrapExternalStorage(narrowName);

	return ShipSystem_NameToSystemId(&ftlName);
}

// This stupid thing has a custom - or at least, non-standard - calling convention
static void DECLSPEC_NAKED __cdecl invokeDrawTextWithGlow(
	int x, int y, bool centred, ftl_std::string *text, GL_Color colour, float alpha
)
{
	// we use 8 bytes of stack before setting ebp, for esp and the stack pointer.
#define ARG_BASE ebp + 8

	// clang-format off
	__asm {
		// What the function takes:
		// 1 int x EDX:4
		// 2 int y ECX:4
		// 3 bool centred Stack[0x4]:1
		// 4 string * text EAX:4
		// 5 Stack[0x8]:16
		// 6 float alpha XMM0_Da:4

		push ebp
		mov ebp, esp

		push edi

		// Push the args right-to-left per the cdecl calling convention
		movss XMM0, [ARG_BASE+32]

		push [ARG_BASE+28] // colour.a
		push [ARG_BASE+24] // colour.b
		push [ARG_BASE+20] // colour.g
		push [ARG_BASE+16] // colour.r

		mov eax, [ARG_BASE+12]
		push [ARG_BASE+8]
		mov ecx, [ARG_BASE+4]
		mov edx, [ARG_BASE+0]

		mov edi, [DrawTextWithGlow]
		call edi

		add esp, 20 // pop the 5 argument values
		pop edi
		pop ebp
		ret
	}
	// clang-format on

#undef ARG_BASE
}

void base_DrawTextWithGlow(int x, int y, bool centred, const std::wstring &text)
{
	std::string narrowText = converter.to_bytes(text);
	ftl_std::string ftlText;
	ftlText.wrapExternalStorage(narrowText);

	GL_Color colour(120 / 255.f, 170 / 255.f, 220 / 255.f, 1.f);
	invokeDrawTextWithGlow(x, y, centred, &ftlText, colour, 1.0f);
}

CompleteShip *base_CreateShip(ShipEvent *event, int sector)
{
	// Get rid of any previous ship
	thishook::callBasegame(WorldManager_ClearLocation, ftlApp->world, 0, nullptr);

	overrideSectorNumber = sector;

	bool isBoss = false;
	DWORD args[] = {(DWORD)event, isBoss};
	CompleteShip *ship = (CompleteShip *)thishook::callBasegame(WorldManager_CreateShip, ftlApp->world, 2, args);

	overrideSectorNumber = -1;

	// We have to re-add the player ship, since ClearLocation removed it.
	// (see CheckLoadEvent for an example)
	DWORD addArgs[] = {(DWORD)ftlApp->world->playerShip->shipManager};
	thishook::callBasegame(SpaceManager_AddShip, &ftlApp->world->space, 1, addArgs);

	return ship;
}

PowerManager *base_GetPowerManager(ShipManager *ship)
{
	return PowerManager_GetPowerManager(ship->iShipId);
}

ShipInfo *GetShipInfo(int shipId)
{
	// It seems this is flipped around
	int index = shipId == 0 ? 1 : 0;

	if (index < ShipInfoList->size())
		return const_cast<ShipInfo *>(&(*ShipInfoList)[index]);
	return nullptr;
}

CompleteShip *GetCompleteShip(Ship *basicShip)
{
	// This is null while the game is loading
	if (ftlApp->world->playerShip == nullptr)
		return nullptr;

	if (&ftlApp->world->playerShip->shipManager->ship == basicShip)
	{
		return ftlApp->world->playerShip;
	}
	for (CompleteShip *ship : ftlApp->world->ships)
	{
		if (&ship->shipManager->ship == basicShip)
			return ship;
	}

	return nullptr;
}

std::wstring ConvertNarrowString(const std::string &narrow)
{
	return converter.from_bytes(narrow);
}

std::string ConvertWideString(const std::wstring &wide)
{
	return converter.to_bytes(wide);
}

std::wstring CrewTaskToName(int taskId)
{
	// clang-format off
	switch (taskId) {
		case 0x0: return L"Manning";
		case 0x1: return L"Repair";
		case 0x2: return L"Fight fire";
		case 0x3: return L"Fix breach";
		case 0x4: return L"Idle";
		case 0x5: return L"Dying";
		case 0x6: return L"Healing";
		case 0x7: return L"Combat";
		case 0x8: return L"Teleport";
		default: return L"Unknown " + std::to_wstring(taskId);
	}
	// clang-format on
}

std::wstring CrewTaskToString(CrewAI *ai, CrewMember *crew, CrewTask task)
{
	int index = ai->crewList.indexOf(crew); // Fine with nullptr

	// Hyperspace doesn't support the -1 index
	if (HasHyperspace && index == -1)
	{
		if (ai->crewList.size() == 0)
			return L"<ERR /w Hyperspace>";

		index = 0;
	}

	DWORD args[] = {(DWORD)task.taskId, (DWORD)task.room, (DWORD)task.system, (DWORD)index};
	int priority = (int)thishook::callBasegame(CrewAI_PrioritizeTask, ai, 4, args);

	std::wstring taskName = CrewTaskToName(task.taskId);
	return taskName + L" room=" + std::to_wstring(task.room) + L" priority=" + std::to_wstring(priority);
}
