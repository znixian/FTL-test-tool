//
// Created by Campbell on 18/06/2023.
//

#pragma once

class OnScreenTimer
{
  public:
	void OnRender();
	void OnLoop();

	bool visible = false;
	bool moving = false;
	bool running = false;

	float seconds = 0.f;

	int x = 760;
	int y = 40;
};
