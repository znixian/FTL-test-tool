//
// This handles hooking functions with the thiscall calling convention.
//
// Created by Campbell on 17/06/2023.
//

#include "common.h"

#include "ThiscallHook.h"

#include "subhook.h"

#include <unordered_map>

// Stop clang-format from splitting this!
#define DECLSPEC_NAKED __declspec(naked)

struct FastCallHook
{
	void *baseFunc = nullptr;
	void *overrideFunc = nullptr;
	subhook::Hook *hook = nullptr;
	int numArgs = 0;
};
static std::unordered_map<void *, FastCallHook> fastcallHooks;

struct FastCallContext
{
	FastCallHook *hook = nullptr;
	void *args = nullptr;
};

// CLion doesn't recognise it's used in inline assembly.
[[maybe_unused]] int __fastcall thiscall_dispatch(void *thisPtr, void *args, DWORD postCallAddr, DWORD *result);

// Unlike other functions, subhook is set to call this function rather
// than jump to it, so we can figure out who called us.
DECLSPEC_NAKED void thiscall_entry()
{
	// ECX=this, other args are on the stack in RTL

	// 4 bytes for the preserved EBP, 4 bytes for the return address
#define ARGS_START 4 + 4

	// clang-format off
	__asm {
		// Safe to clobber: EAX, EDX, ECX (though ECX has the 'this' pointer in it)

		// Grab our caller address
		// Do this first, so (to the debugger) it looks like we were
		// called directly by the target function's caller.
		pop EAX

		// Make LLDB's stack unwinder work properly.
		push EBP
		mov EBP, ESP

		// Find the arguments.
		lea EDX, [EBP+ARGS_START]

		push 0 // Allocate some space for the return value

		// Fastcall, so the two registers are ECX (this) and EDX (args).
		// The next two are the address that called us, and a pointer to put the hook's result.
		push ESP // The dword we just pushed, where the return result will be stored
		push EAX // Our current return address
		call thiscall_dispatch
		mov EDX, EAX // # of dwords to pop

		// We're now done with ECX, and can use it for other stuff

		// Load the return result of the hook function, and don't touch it again since it's
		// our return result, too.
		pop EAX

		// Restore EBP - we shouldn't end up in the debugger from here
		// until the end of the method, so it's fine that it can't properly
		// unwind the stack.
		mov ESP, EBP
		pop EBP

		pop ECX // Return address in ECX

		// Callee clean up, so thiscall_dispatch returned the number of arguments to pop.
		shl EDX, 2 // *4 since we have 32-bit args
		add ESP, EDX

		jmp ECX // Return to caller
	}
	// clang-format on

#undef ARGS_START
}

DECLSPEC_NAKED DWORD __cdecl thiscall_invoke_hook(
	void *fn, FastCallHook *info, int numArgs, void *args, void *thisPtr, FastCallContext *context
)
{
	// clang-format off
	__asm {
		push ecx
		push edx
		push ebp
		mov ebp, esp
		// Argument access:
		// ebp+12  return address
		// ebp+16  fn
		// ebp+20  info
		// ebp+24  numArgs
		// ebp+28  args
		// ebp+32  thisPtr
		// ebp+36  context

		// Hook functions are cdecl, so push their arguments onto the stack, right-to-left
		mov eax, [ebp+24] // numArgs
		mov ecx, [ebp+28] // args - the pointer to the first argument

		// Find the pointer to the last argument
		mov edx, eax
		sub edx, 1 // Want to find the last *valid* value
		shl edx, 2 // *4
		add ecx, edx

		arg_loop:
		test eax, eax
		jz arg_loop_done

		push [ecx]

		sub eax, 1
		sub ecx, 4
		jmp arg_loop
		arg_loop_done:

		push [ebp+32] // Push 'this'
		push [ebp+36] // Push 'context'

		// Call the function
		mov eax, [ebp+16]
		call eax
		// Don't touch eax again, the hook's return result is also our return result

		mov esp, ebp // Get rid of anything left on the stack
		pop ebp
		pop edx
		pop ecx
		ret
	}
	// clang-format on
}

DECLSPEC_NAKED DWORD __cdecl thiscall_invoke_original(void *fn, void *thisPtr, int numArgs, void *args)
{
	// clang-format off
	__asm {
		push ecx
		push edx
		push ebp
		mov ebp, esp
		// Argument access:
		// ebp+12  return address
		// ebp+16  fn
		// ebp+20  thisPtr
		// ebp+24  numArgs
		// ebp+28  args

		mov eax, [ebp+24] // Number of args to copy

		// Find the pointer to the last argument
		mov ecx, [ebp+28]
		mov edx, eax
		sub edx, 1 // Want to find the last *valid* value
		shl edx, 2 // *4
		add ecx, edx

		arg_loop:
		test eax, eax
		jz arg_loop_done

		push [ecx]

		sub eax, 1
		sub ecx, 4
		jmp arg_loop
		arg_loop_done:

		// Load the 'this' pointer
		mov ecx, [ebp+20]

		mov eax, [ebp+16]
		call eax
		// Don't touch eax, the result of the base function is also our result

		mov esp, ebp // Get rid of anything left on the stack
		pop ebp
		pop edx
		pop ecx
		ret
	}
	// clang-format on
}

[[maybe_unused]] int __fastcall thiscall_dispatch(void *thisPtr, void *args, DWORD postCallAddr, DWORD *result)
{
	// Find the function address, by subtracting away the five-byte call instruction.
	DWORD caller = postCallAddr - 5; // Call is one opcode byte plus four address bytes.

	auto iter = fastcallHooks.find((void *)caller);
	if (iter == fastcallHooks.end())
	{
		showError("Unknown hooked function address: (dec) " + std::to_string(postCallAddr));
	}

	FastCallHook *hook = &iter->second;

	FastCallContext context = {
		.hook = hook,
		.args = args,
	};

	*result = thiscall_invoke_hook(hook->overrideFunc, hook, hook->numArgs, args, thisPtr, &context);

	// int testArgs[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	// thiscall_invoke_hook(hook->overrideFunc, hook, 5, testArgs, thisPtr);

	return hook->numArgs;
}

void thishook::addHook(void *baseFunc, void *overrideFunc, int numArgs)
{
	subhook::Hook *hook = new subhook::Hook(baseFunc, (void *)thiscall_entry, subhook::HookFlagCall);
	hook->Install();

	FastCallHook hookInfo = {
		.baseFunc = baseFunc,
		.overrideFunc = overrideFunc,
		.hook = hook,
		.numArgs = numArgs,
	};
	fastcallHooks[baseFunc] = hookInfo;
}

DWORD thishook::callOriginal(FastCallContext *context, void *thisPtr)
{
	return callOriginal(context, thisPtr, (DWORD *)context->args);
}

DWORD thishook::callOriginal(FastCallContext *context, void *thisPtr, DWORD *args)
{
	// Prevent infinite recursion
	subhook::ScopedHookRemove shr(context->hook->hook);

	return thiscall_invoke_original(context->hook->baseFunc, thisPtr, context->hook->numArgs, args);
}

DWORD thishook::callBasegame(void *funcPtr, void *thisPtr, int numArgs, void *args)
{
	return thiscall_invoke_original(funcPtr, thisPtr, numArgs, args);
}
