//
// Created by Campbell on 18/06/2023.
//

#pragma once

#include <memory>

class ControlWindow;
class AbstractDialogue;

class OverviewTab
{
  public:
	OverviewTab(ControlWindow *parent, CApp *app);
	~OverviewTab();

	bool isInGame = false;

	void AddControls(HWND tabWindow);
	bool WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result);
	bool ParentWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result);
	void UpdateUI();

	bool BlockAIWeapons();
	float GetGameSpeed(bool tabPressed);

  private:
	void RunCommand(const std::wstring &command);
	HWND AddRadioButton(HWND tabWindow, int y, const wchar_t *text, int index);

	static LRESULT CALLBACK EditWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	ControlWindow *parent = nullptr;
	CApp *ftlApp = nullptr;

	HWND commandBox = nullptr;

	HWND pirateButton = nullptr;
	HWND upgradeButton = nullptr;
	HWND resourcesButton = nullptr;
	HWND spawnShipButton = nullptr;
	HWND tabToFastforwardButton = nullptr;
	HWND blockEnemyWeaponsButton = nullptr;
	HWND saveGame = nullptr;
	HWND loadGame = nullptr;
	HWND timerVisible = nullptr;
	HWND timerRunning = nullptr;
	HWND resetTimer = nullptr;
	HWND moveTimer = nullptr;

	HWND speedButtons[5] = {};

	int currentSpeed = 2;
	bool tabToFastforward = true;
	bool blockEnemyWeapons = false;
	std::wstring currentCommand;
};
