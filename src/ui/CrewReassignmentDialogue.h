//
// Created by Campbell on 5/07/2023.
//

#pragma once

#include "AbstractDialogue.h"

class CrewReassignmentLog
{
  public:
	std::wstring crewIdent;
	std::wstring oldTask;
	std::wstring newTask;
	int frameNumber = -1; // Set when added
};

class CrewReassignmentDialogue : public AbstractDialogue
{
  public:
	explicit CrewReassignmentDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp);

	void PushLog(CrewReassignmentLog &&log);

	void IncreaseFrameNumber();

  protected:
	void OnInit() override;

	int DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;

  private:
	ShipManager *ship;
	CApp *ftlApp;

	int currentFrameNumber = 1;
	bool hasAddedItems = false;

	HWND mainList = nullptr;

	std::vector<CrewReassignmentLog> items;
};
