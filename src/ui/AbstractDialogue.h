//
// Created by Campbell on 22/06/2023.
//

#pragma once

#include <memory>
#include <vector>

class AbstractDialogue
{
  public:
	AbstractDialogue();
	virtual ~AbstractDialogue();

	AbstractDialogue(const AbstractDialogue &other) = delete;
	AbstractDialogue &operator=(const AbstractDialogue &other) = delete;

	bool IsClosed();
	void Close();
	HWND GetWindowHandle();

	std::shared_ptr<AbstractDialogue> strongRef;

	static std::vector<std::weak_ptr<AbstractDialogue>> allDialogues;

  protected:
	virtual int DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK DialogueProcStatic(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

	void OpenWindow(HWND parentWindow, int classResource);
	void ShowWindowModal(HWND parentWindow, int classResource);

	virtual void OnInit();

	HWND window = nullptr;
};
