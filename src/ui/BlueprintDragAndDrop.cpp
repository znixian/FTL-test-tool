//
// Created by Campbell on 19/06/2023.
//

#include "common.h"

#include "BlueprintDB.h"
#include "BlueprintDragAndDrop.h"
#include "ShipBlueprintsDialogue.h"

#include <ShlObj_core.h>

BlueprintDropSource::BlueprintDropSource() : refCount(1)
{
}

HRESULT BlueprintDropSource::QueryInterface(const IID &riid, void **ppv)
{
	IUnknown *iUkn = nullptr;
	if (riid == IID_IUnknown)
		iUkn = static_cast<IUnknown *>(this);
	else if (riid == IID_IDropSource)
		iUkn = static_cast<IDropSource *>(this);

	*ppv = iUkn;
	if (iUkn)
	{
		iUkn->AddRef();
		return S_OK;
	}
	else
	{
		return E_NOINTERFACE;
	}
}
ULONG BlueprintDropSource::AddRef()
{
	return ++refCount;
}
ULONG BlueprintDropSource::Release()
{
	ULONG refs = --refCount;
	if (refs == 0)
		delete this;
	return refs;
}

HRESULT BlueprintDropSource::QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState)
{
	if (fEscapePressed)
		return DRAGDROP_S_CANCEL;

	if (!(grfKeyState & (MK_LBUTTON | MK_RBUTTON)))
		return DRAGDROP_S_DROP;

	return S_OK;
}
HRESULT BlueprintDropSource::GiveFeedback(DWORD dwEffect)
{
	return DRAGDROP_S_USEDEFAULTCURSORS;
}

// ==================================

BlueprintDataObject::BlueprintDataObject(BlueprintInfo *blueprint) : refCount(1), blueprint(blueprint)
{
	ZeroMemory(formats, sizeof(formats));
	formats[0] = {CF_UNICODETEXT, nullptr, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
}

HRESULT BlueprintDataObject::QueryInterface(const IID &riid, void **ppv)
{
	IUnknown *iUkn = nullptr;
	if (riid == IID_IUnknown)
		iUkn = static_cast<IUnknown *>(this);
	else if (riid == IID_IDataObject)
		iUkn = static_cast<IDataObject *>(this);

	*ppv = iUkn;
	if (iUkn)
	{
		iUkn->AddRef();
		return S_OK;
	}
	else
	{
		return E_NOINTERFACE;
	}
}
ULONG BlueprintDataObject::AddRef()
{
	return ++refCount;
}
ULONG BlueprintDataObject::Release()
{
	ULONG refs = --refCount;
	if (refs == 0)
		delete this;
	return refs;
}

int BlueprintDataObject::GetDataIndex(const FORMATETC *pfe)
{
	for (int i = 0; i < ARRAYSIZE(formats); i++)
	{
		if (pfe->cfFormat == formats[i].cfFormat && (pfe->tymed & formats[i].tymed) &&
		    pfe->dwAspect == formats[i].dwAspect && pfe->lindex == formats[i].lindex)
		{
			return i;
		}
	}
	return DATA_INVALID;
}

HRESULT BlueprintDataObject::CreateHGlobalFromBlob(const void *pvData, SIZE_T cbData, UINT uFlags, HGLOBAL *phglob)
{
	HGLOBAL hglob = GlobalAlloc(uFlags, cbData);
	if (hglob)
	{
		void *pvAlloc = GlobalLock(hglob);
		if (pvAlloc)
		{
			CopyMemory(pvAlloc, pvData, cbData);
			GlobalUnlock(hglob);
		}
		else
		{
			GlobalFree(hglob);
			hglob = nullptr;
		}
	}
	*phglob = hglob;
	return hglob ? S_OK : E_OUTOFMEMORY;
}

HRESULT BlueprintDataObject::GetData(FORMATETC *pfe, STGMEDIUM *pmed)
{
	ZeroMemory(pmed, sizeof(*pmed));

	switch (GetDataIndex(pfe))
	{
	case DATA_TEXT:
		pmed->tymed = TYMED_HGLOBAL;
		return CreateHGlobalFromBlob(blueprint->id.c_str(), blueprint->id.size() * 2, GMEM_MOVEABLE, &pmed->hGlobal);
	}

	return DV_E_FORMATETC;
}

HRESULT BlueprintDataObject::QueryGetData(FORMATETC *pfe)
{
	return GetDataIndex(pfe) == DATA_INVALID ? S_FALSE : S_OK;
}

HRESULT BlueprintDataObject::EnumFormatEtc(DWORD dwDirection, LPENUMFORMATETC *ppefe)
{
	if (dwDirection == DATADIR_GET)
	{
		return SHCreateStdEnumFmtEtc(ARRAYSIZE(formats), formats, ppefe);
	}
	*ppefe = nullptr;
	return E_NOTIMPL;
}

HRESULT BlueprintDataObject::GetDataHere(FORMATETC *pfe, STGMEDIUM *pmed)
{
	return E_NOTIMPL;
}

HRESULT BlueprintDataObject::GetCanonicalFormatEtc(FORMATETC *pfeIn, FORMATETC *pfeOut)
{
	*pfeOut = *pfeIn;
	pfeOut->ptd = nullptr;
	return DATA_S_SAMEFORMATETC;
}

HRESULT BlueprintDataObject::SetData(FORMATETC *pfe, STGMEDIUM *pmed, BOOL fRelease)
{
	return E_NOTIMPL;
}

HRESULT BlueprintDataObject::DAdvise(FORMATETC *pfe, DWORD grfAdv, IAdviseSink *pAdvSink, DWORD *pdwConnection)
{
	return OLE_E_ADVISENOTSUPPORTED;
}

HRESULT BlueprintDataObject::DUnadvise(DWORD dwConnection)
{
	return OLE_E_ADVISENOTSUPPORTED;
}

HRESULT BlueprintDataObject::EnumDAdvise(LPENUMSTATDATA *ppefe)
{
	return OLE_E_ADVISENOTSUPPORTED;
}

// ==================================

BlueprintDropTarget::BlueprintDropTarget() : refCount(1)
{
}

HRESULT BlueprintDropTarget::QueryInterface(const IID &riid, void **ppv)
{
	IUnknown *iUkn = nullptr;
	if (riid == IID_IUnknown)
		iUkn = static_cast<IUnknown *>(this);
	else if (riid == IID_IDropTarget)
		iUkn = static_cast<IDropTarget *>(this);

	*ppv = iUkn;
	if (iUkn)
	{
		iUkn->AddRef();
		return S_OK;
	}
	else
	{
		return E_NOINTERFACE;
	}
}
ULONG BlueprintDropTarget::AddRef()
{
	return ++refCount;
}
ULONG BlueprintDropTarget::Release()
{
	ULONG refs = --refCount;
	if (refs == 0)
		delete this;
	return refs;
}

STDMETHODIMP BlueprintDropTarget::DragEnter(IDataObject *dataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	currentBlueprint = nullptr;

	if (pdwEffect == nullptr)
		return E_INVALIDARG;

	// We don't show an effect until the cursor is over the drop area.
	*pdwEffect = DROPEFFECT_NONE;

	currentBlueprint = GetBlueprint(dataObj);

	return S_OK;
}
STDMETHODIMP BlueprintDropTarget::DragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	if (pdwEffect == nullptr)
		return E_INVALIDARG;

	*pdwEffect = DROPEFFECT_NONE;

	if (currentBlueprint == nullptr)
		return S_OK;

	int dropIdx = FindDropWindowIdx(pt);
	if (dropIdx == -1)
		return S_OK;

	if (dropWindows.at(dropIdx).type != currentBlueprint->type)
		return S_OK;

	*pdwEffect = DROPEFFECT_COPY;
	return S_OK;
}
STDMETHODIMP BlueprintDropTarget::DragLeave()
{
	currentBlueprint = nullptr;
	return S_OK;
}
STDMETHODIMP BlueprintDropTarget::Drop(IDataObject *dataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	if (pdwEffect == nullptr)
		return E_INVALIDARG;

	*pdwEffect = DROPEFFECT_NONE;

	BlueprintInfo *bp = GetBlueprint(dataObj);
	if (bp == nullptr)
		return S_OK;

	int dropIdx = FindDropWindowIdx(pt);
	if (dropIdx == -1)
		return S_OK;

	const BlueprintDropBox &target = dropWindows.at(dropIdx);
	if (target.type != bp->type)
		return S_OK;

	*pdwEffect = DROPEFFECT_COPY;

	*target.blueprintPtr = bp;
	dropListener();

	return S_OK;
}

BlueprintInfo *BlueprintDropTarget::GetBlueprint(IDataObject *dataObj)
{
	BlueprintInfo *blueprint = nullptr;
	std::wstring id;
	int length;

	STGMEDIUM medium = {};
	FORMATETC format = {CF_UNICODETEXT, nullptr, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
	HRESULT result = dataObj->GetData(&format, &medium);
	if (result != S_OK)
		return nullptr;

	HGLOBAL global = medium.hGlobal;
	wchar_t *mem = (wchar_t *)GlobalLock(global);
	int byteSize = (int)GlobalSize(global);

	if (mem == nullptr || byteSize < 2)
		goto end;

	length = byteSize / 2; // size/2 for bytes->chars
	id = std::wstring(mem, length);
	blueprint = BlueprintDB::GetInstance().Lookup(id);

end:
	if (mem != nullptr)
		GlobalUnlock(global);
	ReleaseStgMedium(&medium);
	return blueprint;
}

int BlueprintDropTarget::FindDropWindowIdx(POINTL point)
{
	for (int i = 0; i < dropWindows.size(); i++)
	{
		const BlueprintDropBox &box = dropWindows.at(i);

		RECT rect = {};
		GetWindowRect(box.window, &rect);

		if (rect.left <= point.x && point.x <= rect.right && rect.top <= point.y && point.y <= rect.bottom)
			return i;
	}

	return -1;
}
