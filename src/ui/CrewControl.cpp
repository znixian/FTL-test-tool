//
// Created by Campbell on 18/06/2023.
//

#include "common.h"

#include "CrewControl.h"

#include "FTL.h"
#include "Hooks.h"

#include <algorithm>

CrewControls::CrewControls(CrewMember *crew) : crew(crew)
{
}

bool CrewControls::UpdateUI(ShipManager *ship)
{
	if (!CheckValid(ship))
		return false;

	std::wstring name = base_GetCrewName(crew);
	std::wstring shipId = std::to_wstring(crew->iShipId);
	std::wstring mind = crew->bMindControlled ? L"== YES ==" : L"no";

	ControlWindow::SetText(nameLabel, name);
	ControlWindow::SetText(ownerLabel, shipId);
	ControlWindow::SetText(mindControlled, mind);

	std::wstring taskStr = CrewTaskToName(crew->task.taskId);

	ControlWindow::SetText(task, taskStr);

	std::wstring taskRoomStr = std::to_wstring(crew->task.room);
	ControlWindow::SetText(taskRoom, taskRoomStr);

	return true;
}

int CrewControls::GetOverride()
{
	if (roomOverride < 0 || roomOverride >= crew->ship->vRoomList.size())
		return -1;

	return roomOverride;
}

bool CrewControls::HandleMenu(MenuIDs id, ShipManager *ship)
{
	// Don't crash in testing mode
	if (ship == nullptr)
		return false;

	if (!CheckValid(ship))
		return false;

	switch (id)
	{
	case IDM_HEAL_CREWMEMBER: {
		crew->health.first = crew->health.second;
		return true;
	}
	case IDM_01HP_CREWMEMBER: {
		crew->health.first = 0.1f;
		return true;
	}
	case IDM_KILL_CREWMEMBER: {
		crew->health.first = 0.0f;
		return true;
	}
	case IDM_SWITCH_SIDES: {
		crew->iShipId = 1 - crew->iShipId;
		return true;
	}
	default:
		return false;
	}
}

bool CrewControls::CheckValid(ShipManager *ship)
{
	// Make sure this crew member hasn't been removed and potentially deallocated.
	return std::ranges::any_of(ship->vCrewList, [this](CrewMember *crewMember) { return crew == crewMember; });
}

void CrewControls::ReloadControls()
{
	if (roomOverride == -1)
	{
		ControlWindow::SetText(roomOverrideBox, L"");
	}
	else
	{
		ControlWindow::SetText(roomOverrideBox, std::to_wstring(roomOverride));
	}
}

void CrewControls::RoomOverrideChanged()
{
	std::wstring overrideStr = ControlWindow::GetText(roomOverrideBox);

	if (overrideStr.empty())
	{
		roomOverride = -1;
	}
	else
	{
		roomOverride = _wtoi(overrideStr.c_str());
	}
}
