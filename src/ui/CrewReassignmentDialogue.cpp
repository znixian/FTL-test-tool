//
// Unlike the action log - which shows how all the crew are assigned on
// a single frame - this shows how crew are reassigned between tasks
// across multiple frames.
//
// Created by Campbell on 5/07/2023.
//

#include "common.h"

#include "CrewReassignmentDialogue.h"

#include "../rsrc/resource.h"
#include "ControlWindow.h"
#include "FTL.h"

#include <CommCtrl.h>
#include <windowsx.h>

CrewReassignmentDialogue::CrewReassignmentDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp)
	: ship(ship), ftlApp(ftlApp)
{
	// Re-use the normal log window
	OpenWindow(parentWindow, IDD_CREW_AI_LOG);
}

void CrewReassignmentDialogue::PushLog(CrewReassignmentLog &&log)
{
	log.frameNumber = currentFrameNumber;

	items.emplace_back(std::move(log));

	// Don't pause the game now, in case that interferes
	// with the rest of this frame.
	hasAddedItems = true;

	// Invalidate the list-view, so it repaints
	ListView_SetItemCountEx(mainList, items.size(), 0);
}

void CrewReassignmentDialogue::IncreaseFrameNumber()
{
	currentFrameNumber++;

	// Pause the game, if that check-box is set
	HWND pauseOnChange = GetDlgItem(window, IDC_PAUSE_ON_CHANGE);
	if (hasAddedItems && Button_GetCheck(pauseOnChange))
	{
		ftlApp->gui->bPaused = true;
	}
	hasAddedItems = false;
}

void CrewReassignmentDialogue::OnInit()
{
	AbstractDialogue::OnInit();

	ControlWindow::SetText(window, L"Crew AI Reassignment Log");

	mainList = GetDlgItem(window, IDC_CREW_ACTION_LOG);

	ListView_SetExtendedListViewStyle(mainList, LVS_EX_FULLROWSELECT);

	// Set up the columns
	LVCOLUMN column = {};
	column.mask = LVCF_WIDTH | LVCF_TEXT;
	column.cx = 50;
	column.pszText = (wchar_t *)L"Frame ID";
	ListView_InsertColumn(mainList, 0, &column);
	column.cx = 100;
	column.pszText = (wchar_t *)L"Crew ID";
	ListView_InsertColumn(mainList, 1, &column);
	column.cx = 250;
	column.pszText = (wchar_t *)L"Old Action";
	ListView_InsertColumn(mainList, 2, &column);
	column.cx = 250;
	column.pszText = (wchar_t *)L"New Action";
	ListView_InsertColumn(mainList, 3, &column);
}

int CrewReassignmentDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_NOTIFY: {
		NMHDR *hdr = (NMHDR *)lParam;

		if (hdr->hwndFrom == mainList && hdr->code == LVN_GETDISPINFO)
		{
			NMLVDISPINFO *info = (NMLVDISPINFO *)hdr;
			LVITEM *item = &info->item;

			if (item->iItem < 0 || item->iItem >= items.size())
				return false;

			const CrewReassignmentLog &log = items.at(item->iItem);

			std::wstring result;
			switch (item->iSubItem)
			{
			case 0:
				result = std::to_wstring(log.frameNumber);
				break;
			case 1:
				result = log.crewIdent;
				break;
			case 2:
				result = log.oldTask;
				break;
			case 3:
				result = log.newTask;
				break;
			default:
				break;
			}

			if (item->mask & LVIF_TEXT)
			{
				wcsncpy_s(item->pszText, item->cchTextMax, result.c_str(), item->cchTextMax - 1);
				item->pszText[item->cchTextMax - 1] = 0; // strncpy doesn't null-terminate if too long
			}

			if (item->mask & LVIF_INDENT)
			{
				item->iIndent = 0;
			}

			return true;
		}
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}
