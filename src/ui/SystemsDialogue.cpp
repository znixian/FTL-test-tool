//
// Created by Campbell on 9/07/2023.
//

#include "common.h"

#include "SystemsDialogue.h"

#include "../rsrc/resource.h"
#include "ControlWindow.h"
#include "FTL.h"
#include "Hooks.h"

#include <CommCtrl.h>

// Modal sub-dialogue
class SingleSystemDialogue : public AbstractDialogue
{
  public:
	void Show(HWND parent);

	bool isReactor = false;

	// The index of the system, into the ship's system list.
	// Note this isn't a system ID! (except in UI test mode)
	int systemIdx = 0;

	ShipManager *ship = nullptr;

  protected:
	int DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;
	void OnInit() override;

  private:
	void ApplyChanges();
	void UpdateUsedPower();
	static int GetWindowTextNum(HWND hwnd);

	HWND powerEdit = nullptr, powerSpin = nullptr;
	HWND damageEdit = nullptr, damageSpin = nullptr;
	HWND selectedPowerEdit = nullptr, selectedPowerSpin = nullptr;
};

// ==================
//  Systems dialogue
// ==================

SystemsDialogue::SystemsDialogue(HWND parentWindow, ShipManager *ship) : ship(ship)
{
	OpenWindow(parentWindow, IDD_SYSTEM_POWERS);

	UpdateSystems();
}

SystemsDialogue::~SystemsDialogue() = default;

void SystemsDialogue::OnInit()
{
	AbstractDialogue::OnInit();

	mainView = GetDlgItem(window, IDC_POWER_LIST);

	LVCOLUMN column = {};
	column.mask = LVCF_WIDTH | LVCF_TEXT;
	column.cx = 150;
	column.pszText = (wchar_t *)L"Type";
	ListView_InsertColumn(mainView, 0, &column);
	column.cx = 50;
	column.pszText = (wchar_t *)L"Power";
	ListView_InsertColumn(mainView, 1, &column);
	column.cx = 50;
	column.pszText = (wchar_t *)L"Damage";
	ListView_InsertColumn(mainView, 2, &column);

	ListView_SetExtendedListViewStyle(mainView, LVS_EX_FULLROWSELECT);
}

void SystemsDialogue::UpdateSystems()
{
	// Show all the systems in UI test mode
	int numSystems = ship ? ship->vSystemList.size() : SYS_COUNT;

	// Include the reactor
	numSystems++;

	ListView_SetItemCountEx(mainView, numSystems, 0);
}

void SystemsDialogue::OpenSystemDialogue(int listItemIndex)
{
	SingleSystemDialogue dialogue;
	dialogue.isReactor = listItemIndex == 0;
	dialogue.systemIdx = listItemIndex - 1;
	dialogue.ship = ship;
	dialogue.Show(window);
}

INT_PTR SystemsDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND: {
		int ident = LOWORD(wParam);
		WORD code = HIWORD(wParam);

		if (ident == IDOK && code == BN_CLICKED)
		{
			Close();
			return true;
		}

		break;
	}
	case WM_NOTIFY: {
		NMHDR *hdr = (NMHDR *)lParam;

		if (hdr->hwndFrom == mainView && hdr->code == LVN_GETDISPINFO)
		{
			NMLVDISPINFO *info = (NMLVDISPINFO *)hdr;
			LVITEM *item = &info->item;

			SystemID id;
			int power;
			int damage;

			if (item->iItem == 0)
			{
				// Reactor
				id = SYS_REACTOR_SPECIAL;
				power = ship ? base_GetPowerManager(ship)->currentPower.second : 15;
				damage = 0;
			}
			else
			{
				int numSystems = ship ? ship->vSystemList.size() : SYS_COUNT;

				// -1 because idx=0 is the reactor
				int itemId = item->iItem - 1;
				if (itemId < 0 || itemId >= numSystems)
					return false;

				if (ship)
				{
					ShipSystem *system = ship->vSystemList[itemId];
					id = (SystemID)system->iSystemType;
					power = system->powerState.second;
					damage = system->healthState.second - system->healthState.first;
				}
				else
				{
					// UI test mode
					id = (SystemID)itemId;
					power = 8;
					damage = 0;
				}
			}

			std::wstring text;

			switch (item->iSubItem)
			{
			case 0:
				if (id == SYS_REACTOR_SPECIAL)
					text = L"Reactor";
				else
					text = SYSTEM_NAMES[id];
				break;
			case 1:
				text = std::to_wstring(power);
				break;
			case 2:
				text = std::to_wstring(damage);
				break;
			default:
				return false;
			}

			if (item->mask & LVIF_TEXT)
			{
				wcsncpy_s(item->pszText, item->cchTextMax, text.c_str(), item->cchTextMax - 1);
				item->pszText[item->cchTextMax - 1] = 0; // strncpy doesn't null-terminate if too long
			}

			return true;
		}

		if (hdr->hwndFrom == mainView && hdr->code == NM_CLICK)
		{
			NMITEMACTIVATE *msg = (NMITEMACTIVATE *)hdr;

			// We can't use msg->iItem if any column other than
			// the first is clicked, according to MSDN.

			LVHITTESTINFO hitTest = {};
			hitTest.pt = msg->ptAction;
			ListView_SubItemHitTest(mainView, &hitTest);

			// Didn't click an item?
			if ((hitTest.flags & LVHT_ONITEMLABEL) == 0)
			{
				return false;
			}

			OpenSystemDialogue(hitTest.iItem);
			return true;
		}

		if (hdr->hwndFrom == mainView && hdr->code == NM_RETURN)
		{
			int itemIndex = ListView_GetSelectionMark(mainView);

			// No item selected?
			if (itemIndex == -1)
			{
				return false;
			}

			OpenSystemDialogue(itemIndex);
			return true;
		}

		break;
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}

// ==============================
//  Single system modal dialogue
// ==============================

void SingleSystemDialogue::Show(HWND parent)
{
	ShowWindowModal(parent, IDD_SINGLE_SYSTEM_POWER);

	// The dialogue has now been closed.
}

int SingleSystemDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND: {
		int ident = LOWORD(wParam);
		WORD code = HIWORD(wParam);

		if (ident == IDOK && code == BN_CLICKED)
		{
			ApplyChanges();

			EndDialog(window, 0);
			return true;
		}

		if (ident == IDCANCEL && code == BN_CLICKED)
		{
			EndDialog(window, 0);
			return true;
		}

		break;
	}
	case WM_CLOSE: {
		// Don't use AbstractDialogue's default DestroyWindow
		// handling, as that breaks the UI.
		EndDialog(window, 0);
		return true;
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}

void SingleSystemDialogue::ApplyChanges()
{
	// Don't crash in UI test mode
	if (ship == nullptr)
		return;

	PowerManager *manager = base_GetPowerManager(ship);

	int power = GetWindowTextNum(powerEdit);
	int damage = GetWindowTextNum(damageEdit);
	int selected = GetWindowTextNum(selectedPowerEdit);

	if (isReactor)
	{
		power = min(max(power, 1), 30);

		manager->currentPower.second = power;

		UpdateUsedPower();

		return;
	}

	power = min(max(power, 1), 10);
	damage = min(max(damage, 0), power);
	int remaining = power - damage;
	selected = min(max(selected, 0), remaining);

	ShipSystem *system = ship->vSystemList[systemIdx];

	system->powerState.first = selected;
	system->powerState.second = power;

	system->healthState.first = power - damage;
	system->healthState.second = power;

	UpdateUsedPower();
}

void SingleSystemDialogue::OnInit()
{
	AbstractDialogue::OnInit();

	// Find all the controls
	powerEdit = GetDlgItem(window, IDC_POWER);
	powerSpin = GetDlgItem(window, IDC_POWER_SPIN);
	damageEdit = GetDlgItem(window, IDC_DAMAGE);
	damageSpin = GetDlgItem(window, IDC_DAMAGE_SPIN);
	selectedPowerEdit = GetDlgItem(window, IDC_SELECTED_POWER);
	selectedPowerSpin = GetDlgItem(window, IDC_SELECTED_POWER_SPIN);

	SystemID id;
	int power;
	int damage;
	int selectedPower;

	if (ship)
	{
		if (isReactor)
		{
			id = SYS_REACTOR_SPECIAL;
			power = base_GetPowerManager(ship)->currentPower.second;
			damage = 0;
			selectedPower = 0;
		}
		else
		{
			ShipSystem *sys = ship->vSystemList[systemIdx];
			id = (SystemID)sys->iSystemType;
			power = sys->powerState.second;
			damage = sys->healthState.second - sys->healthState.first;
			selectedPower = sys->powerState.first;
		}
	}
	else
	{
		// UI test mode
		if (isReactor)
		{
			id = SYS_REACTOR_SPECIAL;
			power = 15;
			damage = 0;
			selectedPower = 0;
		}
		else
		{
			id = (SystemID)systemIdx;
			power = 8;
			damage = 0;
			selectedPower = 4;
		}
	}

	// Set the title based on the system
	std::wstring title = L"Edit system: ";
	if (isReactor)
		title += L"Reactor";
	else
		title += SYSTEM_NAMES[id];
	ControlWindow::SetText(window, title);

	// Link up all the edit and up/down controls
	SendMessageW(powerSpin, UDM_SETBUDDY, (WPARAM)powerEdit, 0);
	SendMessageW(damageSpin, UDM_SETBUDDY, (WPARAM)damageEdit, 0);
	SendMessageW(selectedPowerSpin, UDM_SETBUDDY, (WPARAM)selectedPowerEdit, 0);

	if (isReactor)
	{
		LPARAM powerRange = MAKELPARAM(30, 1);
		SendMessageW(powerSpin, UDM_SETRANGE, 0, powerRange);

		SendMessageW(powerSpin, UDM_SETPOS, 0, power);

		EnableWindow(damageSpin, false);
		EnableWindow(damageEdit, false);
		EnableWindow(selectedPowerSpin, false);
		EnableWindow(selectedPowerEdit, false);

		return;
	}

	// Power goes from 1-10
	LPARAM powerRange = MAKELPARAM(10, 1);
	LPARAM zeroRange = MAKELPARAM(10, 0);
	SendMessageW(powerSpin, UDM_SETRANGE, 0, powerRange);
	SendMessageW(damageSpin, UDM_SETRANGE, 0, zeroRange);
	SendMessageW(selectedPowerSpin, UDM_SETRANGE, 0, zeroRange);

	SendMessageW(powerSpin, UDM_SETPOS, 0, power);
	SendMessageW(damageSpin, UDM_SETPOS, 0, damage);
	SendMessageW(selectedPowerSpin, UDM_SETPOS, 0, selectedPower);
}

void SingleSystemDialogue::UpdateUsedPower()
{
	// FTL doesn't re-calculate how much power is in use, so we can
	// end up spawning 'phantom power' that doesn't match up with
	// the number of reactor bars.
	// Thus we need to re-calculate this ourselves.

	PowerManager *manager = base_GetPowerManager(ship);

	int usedPower = 0;

	for (ShipSystem *sys : ship->vSystemList)
	{
		SystemID id = (SystemID)sys->iSystemType;
		if (id == SYS_PILOT || id == SYS_DOORS || id == SYS_SENSORS || id == SYS_BATTERY)
			continue;

		usedPower += sys->powerState.first;
	}

	manager->currentPower.first = usedPower;
}

int SingleSystemDialogue::GetWindowTextNum(HWND hwnd)
{
	std::wstring text = ControlWindow::GetText(hwnd);
	if (text.empty())
		return 0;
	return std::stoi(text);
}
