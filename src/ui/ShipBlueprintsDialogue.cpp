//
// Created by Campbell on 19/06/2023.
//

#include "common.h"

#include "ShipBlueprintsDialogue.h"

#include "../../rsrc/resource.h"
#include "BlueprintDB.h"
#include "BlueprintDragAndDrop.h"
#include "ControlWindow.h"
#include "FTL.h"
#include "Hooks.h"

ShipBlueprintsDialogue::ShipBlueprintsDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp)
	: ship(ship), ftlApp(ftlApp)
{
	// Load the stuff in from the ship, if we're not in test mode.
	if (ship != nullptr)
	{
		if (ship->weaponSystem)
		{
			int id = 0;
			for (ProjectileFactory *weapon : ship->weaponSystem->weapons)
			{
				weapons[id++] = BlueprintDB::GetInstance().Lookup(weapon->blueprint);
				if (id >= ARRAYSIZE(weapons))
					break;
			}
		}

		if (ship->droneSystem)
		{
			int id = 0;
			for (Drone *drone : ship->droneSystem->drones)
			{
				drones[id++] = BlueprintDB::GetInstance().Lookup(drone->blueprint);
				if (id >= ARRAYSIZE(drones))
					break;
			}
		}

		// Blueprints are specified based on how many of them are present
		ShipInfo *info = GetShipInfo(ship->iShipId);
		int id = 0;
		for (const auto &entry : info->augList)
		{
			for (int n = 0; n < entry.value; n++)
			{
				augments[id++] = BlueprintDB::GetInstance().Lookup(entry.key.toWide());
				if (id >= ARRAYSIZE(augments))
					break;
			}
		}
	}

	OpenWindow(parentWindow, IDD_SHIP_BLUEPRINTS);

	UpdateBlueprintButtons();

	dropTarget = new BlueprintDropTarget();
	dropTarget->dropListener = [this]() { UpdateBlueprintButtons(); };
	RegisterDragDrop(window, dropTarget);

	for (int i = 0; i < 4; i++)
	{
		dropTarget->dropWindows.push_back(BlueprintDropTarget::BlueprintDropBox{
			GetDlgItem(window, IDC_WEAPONS_1 + i), BP_TYPE_WEAPON, &weapons[i]});
	}

	for (int i = 0; i < 4; i++)
	{
		dropTarget->dropWindows.push_back(BlueprintDropTarget::BlueprintDropBox{
			GetDlgItem(window, IDC_DRONE_1 + i), BP_TYPE_DRONE, &drones[i]});
	}

	for (int i = 0; i < 3; i++)
	{
		dropTarget->dropWindows.push_back(BlueprintDropTarget::BlueprintDropBox{
			GetDlgItem(window, IDC_AUGMENT_1 + i), BP_TYPE_AUGMENT, &augments[i]});
	}

	slotMenu = CreatePopupMenu();
	AppendMenu(slotMenu, MF_STRING, IDM_CLEAR_EQUIPMENT_SLOT, L"Clear");
}

ShipBlueprintsDialogue::~ShipBlueprintsDialogue()
{
	// Previously we ran RevokeDragDrop(window) here, but when the game
	// is closing this would run *after* COM was uninitialised, which
	// would cause a crash - not that bad if the game is already closing,
	// but still nicer to avoid.

	dropTarget->Release();
	Close();

	DestroyMenu(slotMenu);
}

void ShipBlueprintsDialogue::UpdateBlueprintButtons()
{
	for (int i = 0; i < 4; i++)
	{
		UpdateBlueprintButton(GetDlgItem(window, IDC_WEAPONS_1 + i), weapons[i]);
	}
	for (int i = 0; i < 4; i++)
	{
		UpdateBlueprintButton(GetDlgItem(window, IDC_DRONE_1 + i), drones[i]);
	}
	for (int i = 0; i < 3; i++)
	{
		UpdateBlueprintButton(GetDlgItem(window, IDC_AUGMENT_1 + i), augments[i]);
	}
}

void ShipBlueprintsDialogue::UpdateBlueprintButton(HWND hwnd, BlueprintInfo *blueprint)
{
	if (blueprint == nullptr)
	{
		ControlWindow::SetText(hwnd, L"<empty>");
		return;
	}

	ControlWindow::SetText(hwnd, blueprint->name);
}

INT_PTR ShipBlueprintsDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND: {
		int ident = LOWORD(wParam);
		WORD code = HIWORD(wParam);

		if (ident == IDOK && code == BN_CLICKED)
		{
			Apply();
			Close();
			return true;
		}
		if (ident == IDC_APPLY && code == BN_CLICKED)
		{
			Apply();
			return true;
		}
		if (ident == IDCANCEL && code == BN_CLICKED)
		{
			Close();
			return true;
		}
		if (ident == IDM_CLEAR_EQUIPMENT_SLOT)
		{
			*toClear = nullptr;
			UpdateBlueprintButtons();
		}

		break;
	}
	case WM_RBUTTONDOWN: {
		// Need the screen-relative pos, not the window-relative pos.
		POINT cursor = {};
		GetCursorPos(&cursor);

		// It's a bit of a hack, but this already does what we want.
		POINTL point = {cursor.x, cursor.y};
		int idx = dropTarget->FindDropWindowIdx(point);

		if (idx != -1)
		{
			toClear = dropTarget->dropWindows.at(idx).blueprintPtr;
			ControlWindow::ShowPopupMenu(slotMenu, hwnd);
			return true;
		}

		break;
	}
	default:
		break; // Do nothing.
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}

void ShipBlueprintsDialogue::Apply()
{
	// Don't crash in UI test mode
	if (ship == nullptr)
		return;

	// First clear all the weapons, then re-add them. This stops them
	// from moving around when we're clearing them, and thus leaving
	// some behind that we were supposed to remove.
	// We also have to walk backwards, again to stop the drones we're
	// trying to remove shifting back to indices we've already covered.
	for (int i = 3; i >= 0; i--)
	{
		base_RemoveWeapon(ship, i);
	}
	for (BlueprintInfo *weapon : weapons)
	{
		if (weapon == nullptr)
			continue;

		// Note that if we use i as the slot number, for some reason
		// beams (and possibly other types of weapons) won't fire.
		base_AddWeapon(ship, (WeaponBlueprint *)weapon->blueprint, -1);
	}

	// Same goes for drones.
	// Also note this is safe even if a drones system isn't installed, as
	// these functions do nothing if there isn't a drones system.
	for (int i = 3; i >= 0; i--)
	{
		base_RemoveDrone(ship, i);
	}
	for (BlueprintInfo *drone : drones)
	{
		if (drone != nullptr)
		{
			base_AddDrone(ship, (DroneBlueprint *)drone->blueprint, -1);
		}
	}

	// For augmentations they don't use indices, but do effectively the same thing.
	ShipInfo *shipInfo = GetShipInfo(ship->iShipId);
	while (shipInfo->augCount > 0)
	{
		std::wstring id;
		for (const auto &iter : shipInfo->augList)
		{
			if (iter.value > 0)
			{
				id = iter.key.toWide();
				break;
			}
		}

		// We're marked as still having some augments, but don't have any in the map ?!
		if (id.empty())
			break;

		base_RemoveAugmentation(ship, id);
	}
	for (BlueprintInfo *info : augments)
	{
		if (info == nullptr)
			continue;
		base_AddAugmentation(ship, info->id);
	}

	// AFAIK we only need to run this for the player ship, but it shouldn't hurt
	// to run it for enemy ships too.
	base_CheckEquipmentContents(&ftlApp->gui->equipScreen);

	// We have to tell all the weapons and drones what their targets are. Without
	// this, you can't launch drones that attack the enemy ship, and beams don't fire.
	ShipManager *enemy = nullptr;
	for (CompleteShip *completeShip : ftlApp->world->ships)
	{
		if (completeShip->shipManager == ship)
			continue;
		enemy = completeShip->shipManager;
	}
	if (ship != ftlApp->world->playerShip->shipManager)
	{
		// It seems the player isn't in the world->ships list
		enemy = ftlApp->world->playerShip->shipManager;
	}

	// Passing in null seems to cause a crash later on, when checking for equipment.
	if (enemy != nullptr)
	{
		base_SetShipTarget(ship, enemy);
	}
}
