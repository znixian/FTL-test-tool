//
// Created by Campbell on 18/06/2023.
//

#include "common.h"

#include "ShipCrewTab.h"

#include "CrewActionDialogue.h"
#include "CrewControl.h"
#include "CrewReassignmentDialogue.h"
#include "FTL.h"
#include "Hooks.h"
#include "ShipBlueprintsDialogue.h"
#include "SystemsDialogue.h"

#include <CommCtrl.h>
#include <map>
#include <windowsx.h>

static const wchar_t *SYSTEMS[] = {
	L"shields", L"engines",    L"oxygen",   L"weapons", L"drones",   L"medbay", L"pilot",   L"sensors",
	L"doors",   L"teleporter", L"cloaking", L"battery", L"clonebay", L"mind",   L"hacking",
};
const int NUM_SYSTEMS = ARRAYSIZE(SYSTEMS);

ShipCrewTab::ShipCrewTab(ControlWindow *parent, ShipManager *ship, CApp *app) : parent(parent), ship(ship), ftlApp(app)
{
	// Create our modify-crew popup menu
	menuModifyCrew = CreatePopupMenu();
	AppendMenu(menuModifyCrew, MF_STRING, IDM_HEAL_CREWMEMBER, L"Heal");
	AppendMenu(menuModifyCrew, MF_STRING, IDM_KILL_CREWMEMBER, L"Kill");
	AppendMenu(menuModifyCrew, MF_STRING, IDM_01HP_CREWMEMBER, L"Set 0.1% health");
	AppendMenu(menuModifyCrew, MF_STRING, IDM_SWITCH_SIDES, L"Switch owner ship");

	// Create our add-crew-member popup menu
	menuAddCrew = CreatePopupMenu();
	for (int idx = 0; idx < (IDM_ADD_CREW_MAX_BOUND - IDM_ADD_CREW_HUMAN); idx++)
	{
		AppendMenu(menuAddCrew, MF_STRING, IDM_ADD_CREW_HUMAN + idx, RACES[idx]);
	}

	// Create the add-new-system popup menu
	menuAddSystem = CreatePopupMenu();
	for (int i = 0; i < NUM_SYSTEMS; i++)
	{
		AppendMenu(menuAddSystem, MF_STRING, IDM_SYSTEM_BASE + i, SYSTEMS[i]);
	}
}

ShipCrewTab::~ShipCrewTab()
{
	DestroyMenu(menuModifyCrew);
	DestroyMenu(menuAddCrew);
	DestroyMenu(menuAddSystem);
}

void ShipCrewTab::AddControls(HWND currentTab)
{
	HINSTANCE instance = (HINSTANCE)GetModuleHandle(nullptr);

	parent->CreateLabel(L"Crew name", 30, 30);
	parent->CreateLabel(L"Owner ship", 150, 30);
	parent->CreateLabel(L"Mind controlled", 250, 30);
	parent->CreateLabel(L"Task", 370, 30);
	parent->CreateLabel(L"Task Room", 460, 30);
	parent->CreateLabel(L"Room Override", 540, 30);

	// Note that ship=null if we're in UI test mode
	int numCrew = ship ? ship->vCrewList.size() : 3;

	std::map<CrewMember *, std::unique_ptr<CrewControls>> oldCrew;
	for (std::unique_ptr<CrewControls> &crew : crewControls)
	{
		// This fills crewControls with empty unique_ptrs.
		oldCrew[crew->crew] = std::move(crew);
	}
	crewControls.clear();

	int y = 70;
	for (int i = 0; i < numCrew; i++)
	{
		CrewMember *crewMember = ship ? ship->vCrewList[i] : nullptr;

		// Re-use the previous control objects if possible, to preserve state.
		std::unique_ptr<CrewControls> crew = std::move(oldCrew[crewMember]);
		if (!crew)
		{
			crew = std::make_unique<CrewControls>(crewMember);
		}

		crew->nameLabel = parent->CreateLabel(L"name goes here", 30, y);
		crew->ownerLabel = parent->CreateLabel(L"owner", 150, y);
		crew->mindControlled = parent->CreateLabel(L"mind", 250, y);
		crew->task = parent->CreateLabel(L"task name here", 370, y);
		crew->taskRoom = parent->CreateLabel(L"room", 460, y);

		crew->roomOverrideBox = CreateWindow(
			WC_EDITW,                          // Predefined class
			L"",                               // Label
			WS_VISIBLE | WS_CHILD | ES_NUMBER, // Styles
			540, y, 80, 22,                    // Position and size
			currentTab, nullptr, instance, nullptr
		);
		SendMessage(crew->roomOverrideBox, WM_SETFONT, (WPARAM)parent->generalFont, true);

		crew->moreButton = CreateWindow(
			WC_BUTTONW,            // Predefined class
			L"...",                // Label
			WS_VISIBLE | WS_CHILD, // Styles
			640, y, 30, 22,        // Position and size
			currentTab, nullptr, instance, nullptr
		);
		SendMessage(crew->moreButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

		crew->ReloadControls();

		if (ship)
		{
			crew->UpdateUI(ship);
		}

		crewControls.push_back(std::move(crew));

		y += 30;
	}

	crewActionLogButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Crew AI Action Log", // Label
		WS_VISIBLE | WS_CHILD, // Styles
		30, y, 140, 22,        // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(crewActionLogButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	crewReassignmentLogButton = CreateWindow(
		WC_BUTTONW,                   // Predefined class
		L"Crew AI Re-assignment Log", // Label
		WS_VISIBLE | WS_CHILD,        // Styles
		180, y, 190, 22,              // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(crewReassignmentLogButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	// Add stuff row
	y += 30;
	addCrewButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Add Crew",           // Label
		WS_VISIBLE | WS_CHILD, // Styles
		30, y, 70, 22,         // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(addCrewButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	blockCrewDamageButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Block crew damage",                    // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		105, y, 130, 22,                         // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blockCrewDamageButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(blockCrewDamageButton, blockingCrewDamage);

	addSystemButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Add System",         // Label
		WS_VISIBLE | WS_CHILD, // Styles
		245, y, 120, 22,       // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(addSystemButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	fixSystemsButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Fix Systems",        // Label
		WS_VISIBLE | WS_CHILD, // Styles
		370, y, 120, 22,       // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(fixSystemsButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	editSystemsButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Edit Systems",       // Label
		WS_VISIBLE | WS_CHILD, // Styles
		495, y, 120, 22,       // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(editSystemsButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	// Add the block damage/ion checkboxes
	y += 30;
	blockHullDamageButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Fix hull damage",                      // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		30, y, 120, 22,                          // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blockHullDamageButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(blockHullDamageButton, blockingHullDamage);

	blockIonDamageButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Remove ion/cooldown",                  // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		155, y, 140, 22,                         // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blockIonDamageButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(blockIonDamageButton, blockingIonDamage);

	blockWeaponDamageButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Resist weapons",                       // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		310, y, 140, 22,                         // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blockWeaponDamageButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(blockWeaponDamageButton, blockingWeaponDamage);

	killIntrudersButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Kill intruders",                       // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		460, y, 140, 22,                         // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(killIntrudersButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(killIntrudersButton, killingIntruders);

	// Weapons/drones/augments/cargo stuff
	y += 30;

	blueprintBrowser = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Blueprint Palette",  // Label
		WS_VISIBLE | WS_CHILD, // Styles
		30, y, 130, 22,        // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blueprintBrowser, WM_SETFONT, (WPARAM)parent->generalFont, true);

	shipInventoryButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Ship Inventory",     // Label
		WS_VISIBLE | WS_CHILD, // Styles
		165, y, 120, 22,       // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(shipInventoryButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
}

bool ShipCrewTab::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
	*result = 0; // Default if we handle the event

	switch (uMsg)
	{
	case WM_COMMAND: {
		HWND control = (HWND)lParam; // lParam is the HWND of the clicked button
		MenuIDs ident = (MenuIDs)LOWORD(wParam);
		WORD code = HIWORD(wParam);
		for (int idx = 0; idx < crewControls.size(); idx++)
		{
			const auto &crew = crewControls[idx];
			if (crew->moreButton == control && code == BN_CLICKED)
			{
				ControlWindow::ShowPopupMenu(menuModifyCrew, hwnd);
				openedCrewIdx = idx;
				return true;
			}
			if (crew->roomOverrideBox == control && code == EN_CHANGE)
			{
				crew->RoomOverrideChanged();
				return true;
			}
		}
		if (control == crewActionLogButton && code == BN_CLICKED)
		{
			if (crewActionDialogue)
			{
				crewActionDialogue->Close();
			}

			crewActionDialogue = std::make_unique<CrewActionDialogue>(hwnd, ship, ftlApp);
			crewActionDialogue->strongRef = crewActionDialogue;
			AbstractDialogue::allDialogues.push_back(crewActionDialogue);
			return true;
		}
		if (control == crewReassignmentLogButton && code == BN_CLICKED)
		{
			if (crewReassignmentDialogue)
				crewReassignmentDialogue->Close();

			crewReassignmentDialogue = std::make_unique<CrewReassignmentDialogue>(hwnd, ship, ftlApp);
			crewReassignmentDialogue->strongRef = crewReassignmentDialogue;
			AbstractDialogue::allDialogues.push_back(crewReassignmentDialogue);
			return true;
		}
		if (control == addCrewButton && code == BN_CLICKED)
		{
			ControlWindow::ShowPopupMenu(menuAddCrew, hwnd);
			return true;
		}
		if (control == blockCrewDamageButton && code == BN_CLICKED)
		{
			blockingCrewDamage = Button_GetCheck(blockCrewDamageButton);
			return true;
		}
		if (control == blockHullDamageButton && code == BN_CLICKED)
		{
			blockingHullDamage = Button_GetCheck(blockHullDamageButton);
			return true;
		}
		if (control == blockIonDamageButton && code == BN_CLICKED)
		{
			blockingIonDamage = Button_GetCheck(blockIonDamageButton);
			return true;
		}
		if (control == blockWeaponDamageButton && code == BN_CLICKED)
		{
			blockingWeaponDamage = Button_GetCheck(blockWeaponDamageButton);
			return true;
		}
		if (control == killIntrudersButton && code == BN_CLICKED)
		{
			killingIntruders = Button_GetCheck(killIntrudersButton);
			return true;
		}
		if (control == addSystemButton && code == BN_CLICKED)
		{
			ControlWindow::ShowPopupMenu(menuAddSystem, hwnd);
			return true;
		}
		if (control == fixSystemsButton && code == BN_CLICKED)
		{
			FixAllSystems();
			return true;
		}
		if (control == editSystemsButton && code == BN_CLICKED)
		{
			systemsDialogue = std::make_shared<SystemsDialogue>(hwnd, ship);
			AbstractDialogue::allDialogues.push_back(systemsDialogue);
			// Don't set strongRef, so that when this ship is destroyed the
			// window also closes automatically.
			return true;
		}
		if (control == blueprintBrowser && code == BN_CLICKED)
		{
			parent->OpenBlueprintDialogue();
			return true;
		}
		if (control == shipInventoryButton && code == BN_CLICKED)
		{
			if (shipBlueprints)
			{
				shipBlueprints->Close();
			}

			shipBlueprints = std::make_shared<ShipBlueprintsDialogue>(hwnd, ship, ftlApp);
			shipBlueprints->strongRef = shipBlueprints;
			AbstractDialogue::allDialogues.push_back(shipBlueprints);
			return true;
		}
		if (control == nullptr) // null means a menu sent it
		{
			if (openedCrewIdx >= 0 && openedCrewIdx < crewControls.size())
			{
				bool handled = crewControls[openedCrewIdx]->HandleMenu(ident, ship);
				if (handled)
				{
					return true;
				}
			}
			if (ident >= IDM_ADD_CREW_HUMAN && ident < IDM_ADD_CREW_MAX_BOUND)
			{
				AddCrewMember((int)(ident - IDM_ADD_CREW_HUMAN));
				return true;
			}
			if (ident >= IDM_SYSTEM_BASE && ident < IDM_SYSTEM_BASE + NUM_SYSTEMS)
			{
				int idx = (int)(ident - IDM_SYSTEM_BASE);
				AddSystem(idx);
				return true;
			}
		}
		return false;
	}
	default:
		return false;
	}
}

void ShipCrewTab::AddCrewMember(int raceIdx)
{
	// Don't crash the testing mode
	if (ship == nullptr)
		return;

	std::wstring race = RACES[raceIdx];
	base_AddCrewMember(ship, race, race, false, 0, false, rand() % 2 == 0); // NOLINT(cert-msc50-cpp)
}

void ShipCrewTab::AddSystem(int idx)
{
	// Don't crash the testing mode
	if (ship == nullptr)
		return;

	int systemId = NameToSystemId(SYSTEMS[idx]);
	base_AddSystem(ship, systemId);
}

bool ShipCrewTab::BlockCrewDamage(CrewMember *crew)
{
	return blockingCrewDamage;
}

bool ShipCrewTab::ResistShipDamage()
{
	return blockingWeaponDamage;
}

bool ShipCrewTab::UpdateShip(ShipManager *realShip)
{
	if (ship != realShip)
		return false;

	// Fix all ion/hull damage, if required.
	// It'd be nicer to block it as it came in, but there's no
	// single function that deals with all the hull damage, unfortunately.
	if (blockingHullDamage)
	{
		ship->ship.hullIntegrity.first = ship->ship.hullIntegrity.second;
	}
	if (blockingIonDamage)
	{
		for (ShipSystem *system : ship->vSystemList)
		{
			system->iLockCount = 0;
			system->lockTimer.currTime = 100.f;
		}
	}

	if (crewActionDialogue && crewActionDialogue->IsClosed())
	{
		crewActionDialogue.reset();
	}

	// Have to call this at the end of every update cycle, after the AI stuff is done.
	// Note that this function is run while the game is paused, and if we flip
	// the log while it's paused we'll loose all the messages since there aren't
	// any new ones coming in.
	// Also save the value of bPaused first, since crewActionDialogue can change it.
	bool isPaused = ftlApp->gui->bPaused;
	if (crewActionDialogue && !isPaused)
	{
		crewActionDialogue->FlipLog();
	}
	if (crewReassignmentDialogue && !isPaused)
	{
		crewReassignmentDialogue->IncreaseFrameNumber();
	}

	if (killingIntruders)
	{
		for (CrewMember *crew : ship->vCrewList)
		{
			if (crew->iShipId != ship->iShipId)
			{
				crew->health.first = 0;
			}
		}
	}

	return true;
}

void ShipCrewTab::UpdateUI()
{
	bool valid = crewControls.size() == ship->vCrewList.size();
	for (const auto &control : crewControls)
	{
		valid &= control->UpdateUI(ship);
	}

	if (!valid)
	{
		parent->UpdateTabContents();
	}
}

int ShipCrewTab::GetPriorityOverride(CrewAI *ai, CrewMember *crew)
{
	for (const std::unique_ptr<CrewControls> &control : crewControls)
	{
		if (control->crew != crew)
			continue;

		return control->GetOverride();
	}

	return -1;
}

void ShipCrewTab::PushCrewAIMessage(CrewActionLog &&message)
{
	if (crewActionDialogue)
	{
		crewActionDialogue->PushLog(std::move(message));
	}
}

void ShipCrewTab::PushCrewReassignedMessage(CrewReassignmentLog &&message)
{
	if (crewReassignmentDialogue)
	{
		crewReassignmentDialogue->PushLog(std::move(message));
	}
}

bool ShipCrewTab::OwnsCrew(CrewMember *crew)
{
	return crew->ship == &ship->ship;
}

bool ShipCrewTab::OwnsShip(ShipManager *theShip)
{
	return ship == theShip;
}

void ShipCrewTab::FixAllSystems()
{
	for (ShipSystem *system : ship->vSystemList)
	{
		system->healthState.first = system->healthState.second;
	}
}
