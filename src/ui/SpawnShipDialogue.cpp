//
// Created by Campbell on 26/06/2023.
//

#include "common.h"

#include "SpawnShipDialogue.h"

#include "../rsrc/resource.h"
#include "BlueprintDB.h"
#include "BlueprintDragAndDrop.h"
#include "ControlWindow.h"
#include "FTL.h"
#include "Hooks.h"

#include <CommCtrl.h>
#include <random>
#include <windowsx.h>

SpawnShipDialogue::SpawnShipDialogue(HWND parentWindow)
{
	dropTarget = new BlueprintDropTarget();
	dropTarget->dropListener = [this]() { UpdateUI(); };

	OpenWindow(parentWindow, IDD_SPAWN_SHIP);

	RegisterDragDrop(window, dropTarget);

	dropTarget->dropWindows.emplace_back(BlueprintDropTarget::BlueprintDropBox{
		blueprintInfo,
		BlueprintType::BP_TYPE_SHIP,
		&shipBlueprint,
	});
}

SpawnShipDialogue::~SpawnShipDialogue()
{
	dropTarget->Release();
	Close();
}

void SpawnShipDialogue::OnInit()
{
	blueprintInfo = GetDlgItem(window, IDC_SHIP_BLUEPRINT);

	checkHostile = GetDlgItem(window, IDC_HOSTILE);
	checkSurrender = GetDlgItem(window, IDC_SURRENDER_EN);
	checkEscape = GetDlgItem(window, IDC_ESCAPE_EN);

	numSurrenderHp = GetDlgItem(window, IDC_SURRENDER_HP);
	numEscapeHp = GetDlgItem(window, IDC_ESCAPE_HP);
	numEscapeTimer = GetDlgItem(window, IDC_ESCAPE_TIMER);
	numSector = GetDlgItem(window, IDC_SECTOR);
	numSeed = GetDlgItem(window, IDC_SEED);

	Button_SetCheck(checkHostile, true);

	ControlWindow::SetText(numSurrenderHp, L"5");
	ControlWindow::SetText(numEscapeHp, L"6");
	ControlWindow::SetText(numEscapeTimer, L"30");

	ControlWindow::SetText(numSector, L"3");

	std::random_device rd;
	std::uniform_int_distribution<int> dist(0, INT_MAX);
	int defaultSeed = dist(rd);
	ControlWindow::SetText(numSeed, std::to_wstring(defaultSeed));

	UpdateUI();
}

int SpawnShipDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND: {
		int ident = LOWORD(wParam);
		HWND control = (HWND)lParam; // lParam is the HWND of the clicked button
		WORD code = HIWORD(wParam);

		if (code == BN_CLICKED && (control == checkSurrender || control == checkEscape))
		{
			UpdateUI();
			return true;
		}

		if (ident == IDCANCEL && code == BN_CLICKED)
		{
			Close();
			return true;
		}
		if (ident == IDC_SPAWN && code == BN_CLICKED)
		{
			Spawn();
			// Don't close, in case the user wants to make more changes
			return true;
		}

		break;
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}

void SpawnShipDialogue::UpdateUI()
{
	EnableWindow(numSurrenderHp, Button_GetCheck(checkSurrender));
	EnableWindow(numEscapeHp, Button_GetCheck(checkEscape));
	EnableWindow(numEscapeTimer, Button_GetCheck(checkEscape));

	EnableWindow(GetDlgItem(window, IDC_SPAWN), shipBlueprint != nullptr);

	if (shipBlueprint != nullptr)
	{
		ControlWindow::SetText(blueprintInfo, shipBlueprint->name);
	}
	else
	{
		ControlWindow::SetText(blueprintInfo, L"Drag a blueprint from the palette here");
	}
}

void SpawnShipDialogue::Spawn()
{
	// Shouldn't happen, as the spawn button is greyed out.
	if (shipBlueprint == nullptr)
		return;

	// Sectors are internally 0-indexed, but to the user they're 1-indexed.
	int sector = std::stoi(ControlWindow::GetText(numSector)) - 1;

	std::string narrowBlueprint = ConvertWideString(shipBlueprint->id);

	// We have to use this hack since we don't have the vtable for ShipBlueprint
	uint8_t eventMem[sizeof(ShipEvent)];
	ZeroMemory(eventMem, sizeof(eventMem));
	ShipEvent *event = (ShipEvent *)eventMem;

	event->auto_blueprint.wrapExternalStorage(narrowBlueprint);
	event->hostile = Button_GetCheck(checkHostile);
	event->escapeTimer = std::stoi(ControlWindow::GetText(numEscapeTimer));
	event->shipSeed = std::stoi(ControlWindow::GetText(numSeed));

	int surrenderHp = std::stoi(ControlWindow::GetText(numSurrenderHp));
	if (!Button_GetCheck(checkSurrender))
		surrenderHp = -1;
	event->surrenderThreshold = {surrenderHp, surrenderHp, 0.0f};

	int escapeHp = std::stoi(ControlWindow::GetText(numEscapeHp));
	if (!Button_GetCheck(checkEscape))
		escapeHp = -1;
	event->escapeThreshold = {escapeHp, escapeHp, 0.0f};

	// Copied from the PIRATE ship event in events_ships.xml
	event->surrender.wrapLiteral("PIRATE_SURRENDER");
	event->escape.wrapLiteral("PIRATE_ESCAPE");
	event->destroyed.wrapLiteral("DESTROYED_DEFAULT");
	event->deadCrew.wrapLiteral("DEAD_CREW_DEFAULT");
	// No event ID for gotaway

	base_CreateShip(event, sector);
}
