//
// Created by Campbell on 18/06/2023.
//

#pragma once

#include <memory>
#include <vector>

class CrewControls;
class ControlWindow;
class ShipBlueprintsDialogue;
class CrewActionDialogue;
class CrewReassignmentDialogue;
class CrewActionLog;
class CrewReassignmentLog;
class SystemsDialogue;

class ShipCrewTab
{
  public:
	explicit ShipCrewTab(ControlWindow *parent, ShipManager *ship, CApp *app);
	~ShipCrewTab();

	void AddControls(HWND tabWindow);
	bool WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result);

	/// Returns false if the given ship doesn't match this tab's ship.
	bool UpdateShip(ShipManager *realShip);
	void UpdateUI();
	bool OwnsCrew(CrewMember *crew);
	bool OwnsShip(ShipManager *theShip);

	bool BlockCrewDamage(CrewMember *crew);
	bool ResistShipDamage();
	int GetPriorityOverride(CrewAI *ai, CrewMember *crew);
	void PushCrewAIMessage(CrewActionLog &&message);
	void PushCrewReassignedMessage(CrewReassignmentLog &&message);

  private:
	void AddCrewMember(int raceIdx);
	void AddSystem(int idx);
	void FixAllSystems();

	ControlWindow *parent = nullptr;
	ShipManager *ship = nullptr;
	CApp *ftlApp = nullptr;

	HWND crewActionLogButton = nullptr;
	HWND crewReassignmentLogButton = nullptr;
	HWND addCrewButton = nullptr;
	HWND addSystemButton = nullptr;
	HWND fixSystemsButton = nullptr;
	HWND editSystemsButton = nullptr;
	HWND blockCrewDamageButton = nullptr;
	HWND blockHullDamageButton = nullptr;
	HWND blockIonDamageButton = nullptr;
	HWND blockWeaponDamageButton = nullptr;
	HWND killIntrudersButton = nullptr;
	HWND blueprintBrowser = nullptr;
	HWND shipInventoryButton = nullptr;

	int openedCrewIdx = -1;

	HMENU menuModifyCrew = nullptr;
	HMENU menuAddCrew = nullptr;
	HMENU menuAddSystem = nullptr;

	bool blockingCrewDamage = false;
	bool blockingHullDamage = false;
	bool blockingIonDamage = false;
	bool blockingWeaponDamage = false;
	bool killingIntruders = false;

	std::shared_ptr<CrewActionDialogue> crewActionDialogue;
	std::shared_ptr<CrewReassignmentDialogue> crewReassignmentDialogue;
	std::shared_ptr<ShipBlueprintsDialogue> shipBlueprints;
	std::shared_ptr<SystemsDialogue> systemsDialogue;
	std::vector<std::unique_ptr<CrewControls>> crewControls;
};
