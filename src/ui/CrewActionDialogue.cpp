//
// Created by Campbell on 22/06/2023.
//

#include "common.h"

#include "CrewActionDialogue.h"

#include "../rsrc/resource.h"
#include "FTL.h"
#include "Hooks.h"

#include <CommCtrl.h>
#include <windowsx.h>

CrewActionDialogue::CrewActionDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp) : ship(ship), ftlApp(ftlApp)
{
	OpenWindow(parentWindow, IDD_CREW_AI_LOG);
}

void CrewActionDialogue::OnInit()
{
	AbstractDialogue::OnInit();

	mainList = GetDlgItem(window, IDC_CREW_ACTION_LOG);

	ListView_SetExtendedListViewStyle(mainList, LVS_EX_FULLROWSELECT);

	// Set up the columns
	LVCOLUMN column = {};
	column.mask = LVCF_WIDTH | LVCF_TEXT;
	column.cx = 50;
	column.pszText = (wchar_t *)L"Crew ID";
	ListView_InsertColumn(mainList, 0, &column);
	column.cx = 150;
	column.pszText = (wchar_t *)L"Name";
	ListView_InsertColumn(mainList, 1, &column);
	column.cx = 450;
	column.pszText = (wchar_t *)L"Action";
	ListView_InsertColumn(mainList, 2, &column);
}

void CrewActionDialogue::PushLog(CrewActionLog &&log)
{
	newList.push_back(std::move(log));
}

void CrewActionDialogue::FlipLog()
{
	// Don't update the UI unless something has changed.
	if (currentList == newList)
	{
		newList.clear();
		return;
	}

	currentList.clear();
	std::swap(currentList, newList);

	// Invalidate the list-view, so it repaints
	ListView_SetItemCountEx(mainList, currentList.size(), 0);

	// Pause the game, if that check-box is set
	HWND pauseOnChange = GetDlgItem(window, IDC_PAUSE_ON_CHANGE);
	if (Button_GetCheck(pauseOnChange))
	{
		ftlApp->gui->bPaused = true;
	}
}

int CrewActionDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_NOTIFY: {
		NMHDR *hdr = (NMHDR *)lParam;

		if (hdr->hwndFrom == mainList && hdr->code == LVN_GETDISPINFO)
		{
			NMLVDISPINFO *info = (NMLVDISPINFO *)hdr;
			LVITEM *item = &info->item;

			if (item->iItem < 0 || item->iItem >= currentList.size())
				return false;

			const CrewActionLog &log = currentList.at(item->iItem);
			int index;
			std::wstring crewName;

			if (log.crew == nullptr)
			{
				index = -1;
				crewName = L"";
			}
			else
			{
				// Make sure the crewmember hasn't been deallocated.
				index = ship->vCrewList.indexOf(log.crew);
				if (index == -1)
				{
					crewName = L"INVALID";
				}
				else
				{
					crewName = base_GetCrewName(log.crew);
				}
			}

			std::wstring result;
			switch (item->iSubItem)
			{
			case 0:
				result = index == -1 ? L"-" : std::to_wstring(index);
				break;
			case 1:
				result = crewName;
				break;
			case 2:
				result = log.message;
				break;
			default:
				break;
			}

			if (item->mask & LVIF_TEXT)
			{
				wcsncpy_s(item->pszText, item->cchTextMax, result.c_str(), item->cchTextMax - 1);
				item->pszText[item->cchTextMax - 1] = 0; // strncpy doesn't null-terminate if too long
			}

			if (item->mask & LVIF_INDENT)
			{
				item->iIndent = log.indent;
			}

			return true;
		}
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}

bool CrewActionLog::operator==(const CrewActionLog &other) const
{
	return crew == other.crew && message == other.message;
}
