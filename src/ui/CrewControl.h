//
// Created by Campbell on 18/06/2023.
//

#pragma once

#include "ControlWindow.h"

class CrewControls
{
  public:
	explicit CrewControls(CrewMember *crew);

	CrewControls(const CrewControls &other) = delete;
	CrewControls &operator=(const CrewControls &other) = delete;

	/// UpdateUI the crew, and return false if it's no longer valid.
	bool UpdateUI(ShipManager *ship);

	int GetOverride();
	bool HandleMenu(MenuIDs id, ShipManager *ship);

	/// Call when the control windows have been changed, to re-load old values.
	void ReloadControls();

	void RoomOverrideChanged();

	HWND nameLabel = nullptr;
	HWND ownerLabel = nullptr;
	HWND mindControlled = nullptr;
	HWND task = nullptr;
	HWND taskRoom = nullptr;
	HWND roomOverrideBox = nullptr;
	HWND moreButton = nullptr;

	CrewMember *crew = nullptr;

	int roomOverride = -1;

  private:
	bool CheckValid(ShipManager *ship);
};
