//
// Created by Campbell on 26/06/2023.
//

#pragma once

#include "AbstractDialogue.h"
#include <windef.h>

class BlueprintDropTarget;
class BlueprintInfo;

class SpawnShipDialogue : public AbstractDialogue
{
  public:
	SpawnShipDialogue(HWND parentWindow);
	~SpawnShipDialogue() override;

  protected:
	int DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;
	void OnInit() override;

  private:
	void UpdateUI();
	void Spawn();

	HWND checkHostile = nullptr;
	HWND checkEscape = nullptr;
	HWND checkSurrender = nullptr;

	HWND numSurrenderHp = nullptr;
	HWND numEscapeHp = nullptr;
	HWND numEscapeTimer = nullptr;
	HWND numSector = nullptr;
	HWND numSeed = nullptr;

	HWND blueprintInfo = nullptr;

	BlueprintDropTarget *dropTarget = nullptr;
	BlueprintInfo *shipBlueprint = nullptr;
};
