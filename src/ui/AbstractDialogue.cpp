//
// Created by Campbell on 22/06/2023.
//

#include "common.h"

#include "AbstractDialogue.h"

std::vector<std::weak_ptr<AbstractDialogue>> AbstractDialogue::allDialogues;

AbstractDialogue::AbstractDialogue() = default;

AbstractDialogue::~AbstractDialogue()
{
	Close();
}

void AbstractDialogue::OpenWindow(HWND parentWindow, int classResource)
{
	// This sets window
	CreateDialogParam(GetModInstance(), MAKEINTRESOURCE(classResource), parentWindow, DialogueProcStatic, (LPARAM)this);

	ShowWindow(window, SW_SHOW);
}

void AbstractDialogue::ShowWindowModal(HWND parentWindow, int classResource)
{
	// Blocks until the window is closed
	DialogBoxParam(GetModInstance(), MAKEINTRESOURCE(classResource), parentWindow, DialogueProcStatic, (LPARAM)this);

	// The window has closed at this point
}

bool AbstractDialogue::IsClosed()
{
	return window == nullptr;
}

void AbstractDialogue::Close()
{
	if (window != nullptr)
	{
		DestroyWindow(window);
		window = nullptr;
	}

	// Drop the reference, destroying the window if this
	// is the last reference to it.
	strongRef.reset();
}

HWND AbstractDialogue::GetWindowHandle()
{
	return window;
}

INT_PTR AbstractDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG: {
		// This is called before window is set
		window = hwnd;

		OnInit();
		return true;
	}
	case WM_CLOSE: {
		Close();
		return true;
	}
	case WM_DESTROY: {
		window = nullptr;
		return true;
	}
	default: {
		return false;
	}
	}
}

INT_PTR AbstractDialogue::DialogueProcStatic(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// The 'this' pointer is set in the first message
	if (message == WM_INITDIALOG)
	{
		SetWindowLongPtrW(hwnd, GWLP_USERDATA, lParam);
	}

	AbstractDialogue *dialogue = (AbstractDialogue *)GetWindowLongPtrW(hwnd, GWLP_USERDATA);

	// We get a WM_SETFONT message early on, beefore WM_INITDIALOG.
	// We can't handle these as we don't know the window yet.
	if (dialogue == nullptr)
	{
		return false;
	}

	// Hold onto a shared_ptr to the window, in case this
	// is the close message. This avoids anything happening
	// with a dangling object
	std::shared_ptr<AbstractDialogue> ref = dialogue->strongRef;

	return dialogue->DialogueProc(hwnd, message, wParam, lParam);
}

void AbstractDialogue::OnInit()
{
}
