//
// Created by Campbell on 19/06/2023.
//

#include "common.h"

#include "BlueprintDialogue.h"

#include "../rsrc/resource.h"
#include "BlueprintDB.h"
#include "BlueprintDragAndDrop.h"
#include "ControlWindow.h"
#include "FTL.h"

#include <commctrl.h>

BlueprintDialogue::BlueprintDialogue(HWND parentWindow)
{
	OpenWindow(parentWindow, IDD_BLUEPRINT_SELECTOR);

	UpdateSearch();
}

BlueprintDialogue::~BlueprintDialogue() = default;

void BlueprintDialogue::UpdateSearch()
{
	currentBlueprints.clear();
	for (BlueprintInfo *blueprint : BlueprintDB::GetInstance())
	{
		bool nameMatches = FuzzyCompare(searchTerm, blueprint->name);
		bool idMatches = FuzzyCompare(searchTerm, blueprint->id);
		if (nameMatches || idMatches)
		{
			currentBlueprints.push_back(blueprint);
		}
	}

	ListView_SetItemCountEx(mainView, currentBlueprints.size(), 0);
}

bool BlueprintDialogue::FuzzyCompare(std::wstring query, std::wstring text)
{
	if (query.empty())
		return true;

	// Lower-case the text and query, so searches are case-independent.
	// Note we know there's space for a null, so +1 the sizes.
	_wcslwr_s(query.data(), query.size() + 1);
	_wcslwr_s(text.data(), text.size() + 1);

	// Check each word exists in the text
	for (size_t i = 0; i < query.size();)
	{
		size_t nextSpace = query.find(' ', i);

		// Skip spaces
		if (nextSpace == i)
		{
			i++;
			continue;
		}

		// Find the length of this word
		size_t length;
		if (nextSpace == std::wstring::npos)
		{
			length = query.size() - i;
		}
		else
		{
			length = nextSpace - i;
		}

		std::wstring_view word(query.data() + i, query.data() + i + length);

		// wprintf(L"word: %s len %d\n", query.data() + i, length);
		// fflush(stdout);

		// See if this word exists in the text, and if not we don't match it.
		if (text.find(word) == std::wstring::npos)
		{
			return false;
		}

		i += length + 1;
	}

	return true;
}

void BlueprintDialogue::OnInit()
{
	searchBox = GetDlgItem(window, IDC_SEARCH);
	mainView = GetDlgItem(window, IDC_BLUEPRINT_LIST);

	Edit_SetCueBannerText(searchBox, L"Search");

	LVCOLUMN column = {};
	column.mask = LVCF_WIDTH | LVCF_TEXT;
	column.cx = 150;
	column.pszText = (wchar_t *)L"ID";
	ListView_InsertColumn(mainView, 0, &column);
	column.cx = 250;
	column.pszText = (wchar_t *)L"Name";
	ListView_InsertColumn(mainView, 1, &column);
	column.cx = 100;
	column.pszText = (wchar_t *)L"Type";
	ListView_InsertColumn(mainView, 2, &column);

	ListView_SetExtendedListViewStyle(mainView, LVS_EX_FULLROWSELECT);
}

INT_PTR BlueprintDialogue::DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND: {
		int ident = LOWORD(wParam);
		WORD code = HIWORD(wParam);

		if (ident == IDC_SEARCH && code == EN_CHANGE)
		{
			searchTerm = ControlWindow::GetText(searchBox);
			UpdateSearch();
		}

		break;
	}
	case WM_NOTIFY: {
		NMHDR *hdr = (NMHDR *)lParam;

		if (hdr->hwndFrom == mainView && hdr->code == LVN_GETDISPINFO)
		{
			NMLVDISPINFO *info = (NMLVDISPINFO *)hdr;
			LVITEM *item = &info->item;

			if (item->iItem < 0 || item->iItem >= currentBlueprints.size())
				return false;

			BlueprintInfo *bpInfo = currentBlueprints.at(item->iItem);

			switch (item->iSubItem)
			{
			case 0:
				item->pszText = (wchar_t *)bpInfo->id.c_str();
				break;
			case 1:
				item->pszText = (wchar_t *)bpInfo->name.c_str();
				break;
			case 2:
				item->pszText = (wchar_t *)BLUEPRINT_TYPE_NAMES[(int)bpInfo->type];
				break;
			default:
				return false;
			}

			return true;
		}

		if (hdr->hwndFrom == mainView && hdr->code == LVN_BEGINDRAG)
		{
			NMLISTVIEW *info = (NMLISTVIEW *)hdr;
			int itemId = info->iItem;

			// Sanity-checking
			if (itemId < 0 || itemId >= currentBlueprints.size())
				return false;

			BlueprintInfo *bpInfo = currentBlueprints.at(itemId);

			IDataObject *pdto = new BlueprintDataObject(bpInfo);
			IDropSource *pds = new BlueprintDropSource();
			DWORD dwEffect;
			DoDragDrop(pdto, pds, DROPEFFECT_COPY, &dwEffect);
			pds->Release();
			pdto->Release();

			return true;
		}

		break;
	}
	default:
		break;
	}

	return AbstractDialogue::DialogueProc(hwnd, message, wParam, lParam);
}
