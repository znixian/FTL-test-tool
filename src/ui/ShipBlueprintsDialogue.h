//
// Created by Campbell on 19/06/2023.
//

#pragma once

#include "AbstractDialogue.h"

class BlueprintDropTarget;

class ShipBlueprintsDialogue  : public AbstractDialogue
{
  public:
	ShipBlueprintsDialogue(const ShipBlueprintsDialogue &other) = delete;
	ShipBlueprintsDialogue &operator=(const ShipBlueprintsDialogue &other) = delete;

	ShipBlueprintsDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp);
	~ShipBlueprintsDialogue() override;

	void UpdateBlueprintButtons();

  private:
	INT_PTR DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;
	void Apply();

	static void UpdateBlueprintButton(HWND hwnd, BlueprintInfo *blueprint);

	BlueprintInfo *weapons[4] = {};
	BlueprintInfo *drones[4] = {};
	BlueprintInfo *augments[3] = {};

	ShipManager *ship = nullptr;
	CApp *ftlApp = nullptr;
	BlueprintDropTarget *dropTarget = nullptr;

	HMENU slotMenu = nullptr;
	BlueprintInfo **toClear = nullptr;
};
