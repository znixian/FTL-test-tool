//
// Created by Campbell on 18/06/2023.
//

#include "common.h"

#include "OverviewTab.h"

#include "ControlWindow.h"
#include "FTL.h"
#include "Hooks.h"
#include "OnScreenTimer.h"
#include "SpawnShipDialogue.h"

#include <CommCtrl.h>
#include <windowsx.h>

static const float SPEEDS[] = {0.1f, 0.5f, 1.0f, 2.0f, 5.0f};

OverviewTab::OverviewTab(ControlWindow *parent, CApp *app) : parent(parent), ftlApp(app)
{
	isInGame = parent->isInGame;
}

OverviewTab::~OverviewTab() = default;

void OverviewTab::AddControls(HWND currentTab)
{
	HINSTANCE instance = (HINSTANCE)GetModuleHandle(nullptr);

	// Labels
	parent->CreateLabel(L"In-game", 30, 30);
	parent->CreateLabel(isInGame ? L"Yes" : L"No", 100, 30);

	if (!isInGame)
		return;

	// Command box
	int commandY = 60;
	parent->CreateLabel(L"Console:", 30, commandY);

	commandBox = CreateWindow(
		WC_EDITW,               // Predefined class
		L"",                    // Label
		WS_VISIBLE | WS_CHILD,  // Styles
		100, commandY, 300, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(commandBox, WM_SETFONT, (WPARAM)parent->generalFont, true);
	ControlWindow::SetText(commandBox, currentCommand);
	SetWindowLongPtr(commandBox, GWL_WNDPROC, (LONG)EditWindowProc);

	// Buttons to run pre-set commands, to save typing
	int currentY = 90;

	pirateButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Pirate",             // Label
		WS_VISIBLE | WS_CHILD, // Styles
		30, currentY, 60, 22,  // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(pirateButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	upgradeButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Upgrade",            // Label
		WS_VISIBLE | WS_CHILD, // Styles
		98, currentY, 65, 22,  // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(upgradeButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	resourcesButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Resources",          // Label
		WS_VISIBLE | WS_CHILD, // Styles
		170, currentY, 80, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(resourcesButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	spawnShipButton = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"Spawn Ship",         // Label
		WS_VISIBLE | WS_CHILD, // Styles
		255, currentY, 80, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(spawnShipButton, WM_SETFONT, (WPARAM)parent->generalFont, true);

	// Speed control buttons
	currentY += 30;
	int i = 0;
	speedButtons[0] = AddRadioButton(currentTab, currentY, L"Slowest", i++);
	speedButtons[1] = AddRadioButton(currentTab, currentY, L"Slow", i++);
	speedButtons[2] = AddRadioButton(currentTab, currentY, L"Normal", i++);
	speedButtons[3] = AddRadioButton(currentTab, currentY, L"Fast", i++);
	speedButtons[4] = AddRadioButton(currentTab, currentY, L"Fastest", i++);

	Button_SetCheck(speedButtons[currentSpeed], true);

	// Add the tab-to-speed-up checkbox, and the run-while-unfocused checkbox
	currentY += 30;
	tabToFastforwardButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Holding (Tab) key speeds up",          // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		30, currentY, 200, 22,                   // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(tabToFastforwardButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(tabToFastforwardButton, tabToFastforward);

	// Add the 'block enemy weapons' button
	currentY += 30;
	blockEnemyWeaponsButton = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Block enemy weapon fire",              // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		30, currentY, 200, 22,                   // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(blockEnemyWeaponsButton, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(blockEnemyWeaponsButton, blockEnemyWeapons);

	// Add the save/load game buttons
	currentY += 30;
	saveGame = CreateWindow(
		WC_BUTTONW,            // Predefined class
		L"QuickSave (F11)",    // Label
		WS_VISIBLE | WS_CHILD, // Styles
		30, currentY, 110, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(saveGame, WM_SETFONT, (WPARAM)parent->generalFont, true);

	loadGame = CreateWindow(
		WC_BUTTONW,             // Predefined class
		L"QuickLoad (F12)",     // Label
		WS_VISIBLE | WS_CHILD,  // Styles
		145, currentY, 110, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(loadGame, WM_SETFONT, (WPARAM)parent->generalFont, true);

	// Add the in-game timer controls
	currentY += 30;
	timerVisible = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Show Timer",                           // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		30, currentY, 90, 22,                    // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(timerVisible, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(timerVisible, GameTimer.visible);

	timerRunning = CreateWindow(
		WC_BUTTONW,                              // Predefined class
		L"Run Timer (F9)",                       // Label
		WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, // Styles
		125, currentY, 110, 22,                  // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(timerRunning, WM_SETFONT, (WPARAM)parent->generalFont, true);
	Button_SetCheck(timerRunning, GameTimer.running);

	resetTimer = CreateWindow(
		WC_BUTTONW,             // Predefined class
		L"Reset Timer (F10)",   // Label
		WS_VISIBLE | WS_CHILD,  // Styles
		240, currentY, 120, 22, // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(resetTimer, WM_SETFONT, (WPARAM)parent->generalFont, true);

	moveTimer = CreateWindow(
		WC_BUTTONW,                  // Predefined class
		L"Move Timer (ESC to stop)", // Label
		WS_VISIBLE | WS_CHILD,       // Styles
		365, currentY, 160, 22,      // Position and size
		currentTab, nullptr, instance, nullptr
	);
	SendMessage(moveTimer, WM_SETFONT, (WPARAM)parent->generalFont, true);
}

HWND OverviewTab::AddRadioButton(HWND tabWindow, int y, const wchar_t *text, int index)
{
	int x = 30 + 70 * index;
	HINSTANCE instance = (HINSTANCE)GetModuleHandle(nullptr);

	int style = WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON;
	if (index == 0)
		style |= WS_GROUP;

	HWND button = CreateWindow(
		WC_BUTTONW,   // Predefined class
		text,         // Label
		style,        // Styles
		x, y, 65, 22, // Position and size
		tabWindow, nullptr, instance, nullptr
	);
	SendMessage(button, WM_SETFONT, (WPARAM)parent->generalFont, true);

	return button;
}

void OverviewTab::UpdateUI()
{
	// This can be toggled via the F9 key
	if (Button_GetCheck(timerRunning) != GameTimer.running)
	{
		Button_SetCheck(timerRunning, GameTimer.running);
	}
}

bool OverviewTab::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
	*result = 0; // Default result

	switch (uMsg)
	{
	case WM_COMMAND: {
		HWND control = (HWND)lParam;
		MenuIDs ident = (MenuIDs)LOWORD(wParam);
		WORD code = HIWORD(wParam);

		if (control == commandBox && code == EN_CHANGE)
		{
			currentCommand = ControlWindow::GetText(commandBox);
			return true;
		}

		if (control == pirateButton && code == BN_CLICKED)
		{
			RunCommand(L"EVENT PIRATE");
		}
		if (control == upgradeButton && code == BN_CLICKED)
		{
			RunCommand(L"GOD");
		}
		if (control == resourcesButton && code == BN_CLICKED)
		{
			RunCommand(L"RICH");
		}
		if (control == spawnShipButton && code == BN_CLICKED)
		{
			std::shared_ptr<AbstractDialogue> spawnShip(new SpawnShipDialogue(hwnd));
			spawnShip->strongRef = spawnShip;
			AbstractDialogue::allDialogues.push_back(spawnShip);
		}

		for (int i = 0; i < ARRAYSIZE(speedButtons); i++)
		{
			if (control == speedButtons[i] && code == BN_CLICKED)
			{
				currentSpeed = i;
			}
		}
		if (control == tabToFastforwardButton && code == BN_CLICKED)
		{
			tabToFastforward = Button_GetCheck(tabToFastforwardButton);
		}

		if (control == blockEnemyWeaponsButton && code == BN_CLICKED)
		{
			blockEnemyWeapons = Button_GetCheck(blockEnemyWeaponsButton);
		}

		if (control == saveGame && code == BN_CLICKED && ftlApp != nullptr)
		{
			QuickSave();
		}
		if (control == loadGame && code == BN_CLICKED && ftlApp != nullptr)
		{
			QuickLoad();
		}

		if (control == timerVisible && code == BN_CLICKED)
		{
			GameTimer.visible = Button_GetCheck(timerVisible);
		}
		if (control == timerRunning && code == BN_CLICKED)
		{
			GameTimer.running = Button_GetCheck(timerRunning);
		}
		if (control == resetTimer && code == BN_CLICKED)
		{
			GameTimer.seconds = 0.f;
		}
		if (control == moveTimer && code == BN_CLICKED)
		{
			GameTimer.moving = true;
		}

		return false;
	}
	default:
		return false;
	}
}

bool OverviewTab::ParentWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result)
{
	*result = 0; // Default result

	switch (uMsg)
	{
	case WM_KEYDOWN: {
		if (GetFocus() == commandBox && wParam == VK_RETURN)
		{
			RunCommand(currentCommand);
			ControlWindow::SetText(commandBox, L"");
			currentCommand = L"";
			return true;
		}

		return false;
	}
	default:
		return false;
	}
}

void OverviewTab::RunCommand(const std::wstring &command)
{
	// Don't crash in UI test mode
	if (ftlApp == nullptr)
		return;

	base_RunCommand(ftlApp->gui, command);
}

LRESULT OverviewTab::EditWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// This is set on the edit box, so we can skip it's return keydown event and
	// instead send it to the parent window.
	if (uMsg == WM_KEYDOWN && wParam == VK_RETURN)
	{
		HWND parent = GetAncestor(hwnd, GA_ROOT);
		WNDPROC proc = (WNDPROC)GetWindowLongPtr(parent, GWL_WNDPROC);
		return CallWindowProc(proc, parent, uMsg, wParam, lParam);
	}

	HINSTANCE instance = (HINSTANCE)GetModuleHandle(nullptr);

	WNDCLASSW info;
	GetClassInfo(instance, WC_EDITW, &info);
	return CallWindowProc(info.lpfnWndProc, hwnd, uMsg, wParam, lParam);
}

bool OverviewTab::BlockAIWeapons()
{
	return blockEnemyWeapons;
}

float OverviewTab::GetGameSpeed(bool tabPressed)
{
	if (tabToFastforward && tabPressed)
	{
		return 5.f;
	}

	return SPEEDS[currentSpeed];
}
