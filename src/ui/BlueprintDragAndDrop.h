//
// See Raymond Chen's series on drag and drop:
// https://devblogs.microsoft.com/oldnewthing/20041206-00/?p=37133
//
// This is heavily based off those examples.
//
// Created by Campbell on 19/06/2023.
//

#pragma once

#include <functional>
#include <oleidl.h>
#include <vector>

class ShipBlueprintsDialogue;

class BlueprintDropSource : public IDropSource
{
  public:
	// *** IUnknown ***
	STDMETHODIMP QueryInterface(REFIID riid, void **ppv) override;
	STDMETHODIMP_(ULONG) AddRef() override;
	STDMETHODIMP_(ULONG) Release() override;

	// *** IDropSource ***
	STDMETHODIMP QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState) override;
	STDMETHODIMP GiveFeedback(DWORD dwEffect) override;

	BlueprintDropSource();

  private:
	ULONG refCount;
};

class BlueprintDataObject : public IDataObject
{
  public:
	// IUnknown
	STDMETHODIMP QueryInterface(REFIID riid, void **ppvObj) override;
	STDMETHODIMP_(ULONG) AddRef() override;
	STDMETHODIMP_(ULONG) Release() override;

	// IDataObject
	STDMETHODIMP GetData(FORMATETC *pfe, STGMEDIUM *pmed) override;
	STDMETHODIMP GetDataHere(FORMATETC *pfe, STGMEDIUM *pmed) override;
	STDMETHODIMP QueryGetData(FORMATETC *pfe) override;
	STDMETHODIMP GetCanonicalFormatEtc(FORMATETC *pfeIn, FORMATETC *pfeOut) override;
	STDMETHODIMP SetData(FORMATETC *pfe, STGMEDIUM *pmed, BOOL fRelease) override;
	STDMETHODIMP EnumFormatEtc(DWORD dwDirection, LPENUMFORMATETC *ppefe) override;
	STDMETHODIMP DAdvise(FORMATETC *pfe, DWORD grfAdv, IAdviseSink *pAdvSink, DWORD *pdwConnection) override;
	STDMETHODIMP DUnadvise(DWORD dwConnection) override;
	STDMETHODIMP EnumDAdvise(LPENUMSTATDATA *ppefe) override;

	explicit BlueprintDataObject(BlueprintInfo *blueprint);

  private:
	enum
	{
		DATA_TEXT,
		DATA_NUM,
		DATA_INVALID = -1,
	};

	int GetDataIndex(const FORMATETC *pfe);
	static HRESULT CreateHGlobalFromBlob(const void *pvData, SIZE_T cbData, UINT uFlags, HGLOBAL *phglob);

  private:
	ULONG refCount;
	FORMATETC formats[DATA_NUM];
	BlueprintInfo *blueprint = nullptr;
};

class BlueprintDropTarget : public IDropTarget
{
  public:
	struct BlueprintDropBox
	{
		HWND window = nullptr;
		int /* BlueprintType */ type = 0;
		BlueprintInfo **blueprintPtr = nullptr;
	};

	// *** IUnknown ***
	STDMETHODIMP QueryInterface(REFIID riid, void **ppv) override;
	STDMETHODIMP_(ULONG) AddRef() override;
	STDMETHODIMP_(ULONG) Release() override;

	// *** IDropTarget ***
	STDMETHODIMP DragEnter(IDataObject *dataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) override;
	STDMETHODIMP DragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) override;
	STDMETHODIMP DragLeave() override;
	STDMETHODIMP Drop(IDataObject *dataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) override;

	BlueprintDropTarget();

	std::vector<BlueprintDropBox> dropWindows;
	std::function<void()> dropListener;

	int FindDropWindowIdx(POINTL point);

  private:
	static BlueprintInfo *GetBlueprint(IDataObject *dataObj);

	BlueprintInfo *currentBlueprint = nullptr;
	ULONG refCount;
};
