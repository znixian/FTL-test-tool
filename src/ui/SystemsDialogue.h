//
// Created by Campbell on 9/07/2023.
//

#pragma once

#include "AbstractDialogue.h"

class SystemsDialogue : public AbstractDialogue
{
  public:
	SystemsDialogue(HWND parentWindow, ShipManager *ship);
	~SystemsDialogue() override;

  protected:
	void OnInit() override;

  private:
	HWND mainView = nullptr;
	ShipManager *ship = nullptr;

	void UpdateSystems();
	void OpenSystemDialogue(int listItemIndex);

	INT_PTR DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;
};
