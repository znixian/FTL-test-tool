//
// Created by Campbell on 22/06/2023.
//

#pragma once

#include "AbstractDialogue.h"

#include <vector>

class CrewActionLog
{
  public:
	CrewMember *crew = nullptr; // nullptr means not crew-specific
	std::wstring message;
	int indent = 0;

	bool operator==(const CrewActionLog &other) const;
};

class CrewActionDialogue : public AbstractDialogue
{
  public:
	explicit CrewActionDialogue(HWND parentWindow, ShipManager *ship, CApp *ftlApp);

	void PushLog(CrewActionLog &&log);

	// Marks this frame's updates as finished, and displays
	// the log entries added this frame.
	void FlipLog();

  protected:
	void OnInit() override;

	int DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;

  private:
	ShipManager *ship;
	CApp *ftlApp;

	HWND mainList = nullptr;

	std::vector<CrewActionLog> currentList;
	std::vector<CrewActionLog> newList;
};
