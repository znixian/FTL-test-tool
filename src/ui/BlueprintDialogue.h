//
// Created by Campbell on 19/06/2023.
//

#pragma once

#include "AbstractDialogue.h"

#include <vector>

class BlueprintDialogue : public AbstractDialogue
{
  public:
	BlueprintDialogue(const BlueprintDialogue &other) = delete;
	BlueprintDialogue &operator=(const BlueprintDialogue &other) = delete;

	explicit BlueprintDialogue(HWND parentWindow);
	~BlueprintDialogue() override;

  private:
	INT_PTR DialogueProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;

	void UpdateSearch();

  protected:
	void OnInit() override;

  private:
	HWND searchBox = nullptr;
	HWND mainView = nullptr;

	std::wstring searchTerm;
	std::vector<BlueprintInfo *> currentBlueprints;

	bool FuzzyCompare(std::wstring query, std::wstring text);
};
