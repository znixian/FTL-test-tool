//
// Created by Campbell on 17/06/2023.
//

#include "common.h"

#include "FunctionPointers.h"

#include "Hooks.h"

static void dontUnload();

static HINSTANCE instance = nullptr;

BOOL WINAPI DllMain(
	HINSTANCE hinstDLL, // handle to DLL module
	DWORD fdwReason,    // reason for calling function
	LPVOID lpvReserved
) // reserved
{
	// Perform actions based on the reason for calling.
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		// Initialize once for each new process.
		// Return FALSE to fail DLL load.
		FunctionPointers::Init();
		InstallHooks();
		dontUnload();
		instance = hinstDLL;
		break;

	case DLL_THREAD_ATTACH:
		// Do thread-specific initialization.
		break;

	case DLL_THREAD_DETACH:
		// Do thread-specific cleanup.
		break;

	case DLL_PROCESS_DETACH:

		if (lpvReserved != nullptr)
		{
			break; // do not do cleanup if process termination scenario
		}

		// Perform any necessary cleanup.
		break;
	}
	return TRUE; // Successful DLL_PROCESS_ATTACH.
}

void dontUnload()
{
	// FTL will unload us since we don't export any of the necessary functions,
	// so increment our load counter so we stick around.

	// We're absolutely NOT supposed to do this, due to the DLL loading mutex,
	// but play with fire anyway since we know it won't cause a new DLL to load.
	LoadLibraryW(L"hid.dll");
}

HINSTANCE GetModInstance()
{
	return instance;
}
