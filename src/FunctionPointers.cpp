//
// Created by Campbell on 17/06/2023.
//

#include "common.h"

#include "FunctionPointers.h"

#include <fstream>
#include <memory>
#include <vector>

#include <Windows.h>

// Singleton handling
static std::unique_ptr<FunctionPointers> instance;

void FunctionPointers::Init()
{
	instance = std::unique_ptr<FunctionPointers>(new FunctionPointers);
}

FunctionPointers &FunctionPointers::Instance()
{
	return *instance;
}

// Main code

static void readExecutable(std::vector<uint8_t> &output, HMODULE module);

FunctionPointers::FunctionPointers()
{
	HMODULE ftlModule = GetModuleHandleW(nullptr);

	// Read in the full file somewhere, so we can access unmapped regions
	// Mapping it would save 100MiB of memory, but who cares.
	std::vector<uint8_t> image;
	readExecutable(image, ftlModule);
	uint8_t *basePtr = (uint8_t *)image.data();

	IMAGE_NT_HEADERS *headers = (IMAGE_NT_HEADERS *)(basePtr + *(uint32_t *)(basePtr + 0x3c));
	if (headers->Signature != IMAGE_NT_SIGNATURE)
	{
		showError("Missing PE header");
	}
	IMAGE_SECTION_HEADER *sections = IMAGE_FIRST_SECTION(headers);

	IMAGE_SYMBOL *imageSymbolTable = (IMAGE_SYMBOL *)(headers->FileHeader.PointerToSymbolTable + basePtr);
	const char *stringTable = (const char *)&imageSymbolTable[headers->FileHeader.NumberOfSymbols];

	// for (int i = 0; i < headers->FileHeader.NumberOfSections; i++) {
	// 	IMAGE_SECTION_HEADER *section = &sections[i];
	// }

	bool dumpSymbols = GetEnvironmentVariableW(L"FTL_TEST_DUMP_SYMS", nullptr, 0) != 0;

	std::ofstream symbolFile;
	if (dumpSymbols)
	{
		symbolFile.open("symbols.txt");
	}

	for (int i = 0; i < (int)headers->FileHeader.NumberOfSymbols; i++)
	{
		IMAGE_SYMBOL symbol = imageSymbolTable[i];

		std::string name;
		if (symbol.N.Name.Short == 0)
		{
			const char *longName = stringTable + symbol.N.Name.Long;
			name = std::string(longName);
		}
		else
		{
			char buf[9];
			ZeroMemory(buf, sizeof(buf));
			memcpy(buf, symbol.N.ShortName, 8);
			name = std::string(buf);
		}

		// Don't care about non-address symbols
		bool isFnSym = symbol.StorageClass == IMAGE_SYM_CLASS_EXTERNAL || symbol.StorageClass == IMAGE_SYM_CLASS_STATIC;
		if (symbol.SectionNumber <= 0 || !isFnSym)
		{
			if (dumpSymbols)
			{
				symbolFile << std::dec << i << " Skipped"
						   << "\t" << name << std::endl;
			}

			continue;
		}

		DWORD rva = sections[symbol.SectionNumber - 1].VirtualAddress + symbol.Value;

		uint8_t *address = (uint8_t *)ftlModule + rva;
		symbolTable[name] = address;

		if (dumpSymbols)
		{
			symbolFile << std::hex << (int)address << "\t" << name << std::endl;
		}

		// Skip the aux symbols
		i += symbol.NumberOfAuxSymbols;
	}
}

void *FunctionPointers::LookupSymbol(const std::string &sym) const
{
	auto iter = symbolTable.find(sym);
	if (iter == symbolTable.end())
	{
		showError("Missing symbol: " + sym);
	}
	return iter->second;
}

FunctionPointers::~FunctionPointers() = default;

// Section loader

[[noreturn]] void showError(const std::string &message)
{
	MessageBoxA(nullptr, message.c_str(), "FTL Test Tool - unknown section type", MB_OK);
	ExitProcess(1);
}

void readExecutable(std::vector<uint8_t> &output, HMODULE module)
{
	wchar_t filename[MAX_PATH + 1];
	ZeroMemory(filename, sizeof(filename));
	DWORD filenameLength = GetModuleFileNameW(module, filename, MAX_PATH);
	if (filenameLength == 0)
	{
		showError("Failed to load FTL executable path");
	}

	std::ifstream ftlFile(filename, std::ios::binary); // creating from wstring is MSVC extension
	ftlFile.seekg(0, std::ios::end);
	int exeSize = (int)ftlFile.tellg();
	output.resize(exeSize);
	ftlFile.seekg(0, std::ios::beg);

	ftlFile.read((char *)output.data(), output.size());

	if (ftlFile.bad())
	{
		showError("Failed to read FTL executable");
	}
}
