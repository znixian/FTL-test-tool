//
// Created by Campbell on 18/06/2023.
//

#include "common.h"

#include "OnScreenTimer.h"

#include "FTL.h"
#include "Hooks.h"

void OnScreenTimer::OnRender()
{
	wchar_t buf[100];
	int ms = (int)(seconds * 1000) % 1000;
	swprintf_s(buf, L"%d.%03d", (int)seconds, ms);

	std::wstring message = buf;

	if (!running)
	{
		// Add (P) for Paused
		message += L" (P)";
	}

	base_DrawTextWithGlow(x, y, false, message);

	if (moving)
	{
		x = TheGameMouse->position.x;
		y = TheGameMouse->position.y;
	}
}

void OnScreenTimer::OnLoop()
{
	if (running)
	{
		seconds += FPSControl->SpeedFactor / 16.0f;
	}
}
