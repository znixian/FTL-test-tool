//
// Created by Campbell on 17/06/2023.
//

#pragma once

#define UNICODE
#define WIN32_LEAN_AND_MEAN
#define _CRT_NONSTDC_NO_WARNINGS
#define ISOLATION_AWARE_ENABLED 1
#include <Windows.h>

#include <string>

[[noreturn]] void showError(const std::string &message);

// Forward declarations for some widely-used FTL stuff
struct CApp;
struct CrewAI;
struct CrewMember;
struct ShipManager;
struct Blueprint;

// This is from BlueprintDB
struct BlueprintInfo;

// Get the instance of our DLL, which we need when we're using resource files.
HINSTANCE GetModInstance();

std::wstring ConvertNarrowString(const std::string &narrow);
std::string ConvertWideString(const std::wstring &wide);

namespace ftl_std
{
struct string;
}
