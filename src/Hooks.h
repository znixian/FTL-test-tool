//
// Created by Campbell on 17/06/2023.
//

#pragma once

struct Ship;
struct CompleteShip;
struct ShipManager;
struct CrewMember;
struct CommandGui;
struct CFPS;
struct MouseControl;
struct BlueprintManager;
struct ShipInfo;
struct Equipment;
struct WeaponSystem;
struct WeaponBlueprint;
struct DroneBlueprint;
struct AugmentBlueprint;
struct CrewTask;
struct ShipEvent;
struct TextString;
struct PowerManager;

// Global objects
extern CFPS *FPSControl;
extern MouseControl *TheGameMouse;
extern BlueprintManager *TheBlueprintManager;

// This is a custom thing
extern class OnScreenTimer GameTimer;
extern bool HasHyperspace;

void InstallHooks();

CrewMember *base_AddCrewMember(
	ShipManager *ship, const std::wstring &name, const std::wstring &type, bool intruder, int roomId, bool init,
	bool male
);
void base_AddSystem(ShipManager *ship, int systemId);

void base_RunCommand(CommandGui *gui, const std::wstring &command);

std::wstring base_GetCrewName(CrewMember *crew);
std::wstring base_GetTextString(const TextString *string);

void base_CheckEquipmentContents(Equipment *equipment);

void base_AddWeapon(ShipManager *ship, WeaponBlueprint *blueprint, int slot);
void base_RemoveWeapon(ShipManager *ship, int slot);

void base_AddDrone(ShipManager *ship, DroneBlueprint *blueprint, int slot);
void base_RemoveDrone(ShipManager *ship, int slot);

void base_AddAugmentation(ShipManager *ship, const std::wstring &id);
void base_RemoveAugmentation(ShipManager *ship, const std::wstring &id);

void base_SetShipTarget(ShipManager *ship, ShipManager *target);

void base_DrawTextWithGlow(int x, int y, bool centred, const std::wstring &text);

CompleteShip *base_CreateShip(ShipEvent *event, int sector);

PowerManager *base_GetPowerManager(ShipManager *ship);

ShipInfo *GetShipInfo(int shipId);
CompleteShip *GetCompleteShip(Ship *basicShip);

void MemFreeFTL(void *memory);
void *MemNewFTL(int size);

void QuickSave();
void QuickLoad();

int NameToSystemId(const std::wstring &systemName);
std::wstring CrewTaskToName(int taskId);
std::wstring CrewTaskToString(CrewAI *ai, CrewMember *crew, CrewTask task);
