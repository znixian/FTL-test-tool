//
// Created by Campbell on 9/07/2023.
//

#include "common.h"

#include "FTL.h"

const wchar_t *SYSTEM_NAMES[SYS_COUNT] = {
	// clang-format off
	L"Shields",
	L"Engines",
	L"Oxygen",
	L"Weapons",
	L"Drone",
	L"Medbay",
	L"Pilot",
	L"Sensors",
	L"Doors",
	L"Teleporter",
	L"Cloaking",
	L"Artillery",
	L"Battery",
	L"Clonebay",
	L"Mind Control",
	L"Hacking",
	// clang-format on
};
