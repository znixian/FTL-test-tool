//
// Created by Campbell on 17/06/2023.
//

#pragma once

#include <memory>
#include <vector>

class ShipCrewTab;
class OverviewTab;
class BlueprintDialogue;
class CrewActionLog;
class CrewReassignmentLog;

enum MenuIDs
{
	IDM_HEAL_CREWMEMBER = 100,
	IDM_KILL_CREWMEMBER,
	IDM_01HP_CREWMEMBER,
	IDM_SWITCH_SIDES,

	IDM_ADD_CREW_HUMAN = 200,
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
	IDM_ADD_CREW_SLUG,
	IDM_ADD_CREW_ENGI,
	IDM_ADD_CREW_MANTIS,
	IDM_ADD_CREW_ENERGY,
	IDM_ADD_CREW_ROCK,
	IDM_ADD_CREW_CRYSTAL,
	IDM_ADD_CREW_ANAEROBIC,
#pragma clang diagnostic pop
	IDM_ADD_CREW_MAX_BOUND,

	IDM_SYSTEM_BASE, // Systems come after this

	IDM_CLEAR_EQUIPMENT_SLOT = 300,
};

extern const wchar_t *RACES[];

class ControlWindow
{
  public:
	ControlWindow();
	~ControlWindow();

	void Update(CApp *app);
	void UpdateIntruders(CrewAI *ai);
	int GetPriorityOverride(CrewAI *ai, CrewMember *crew);
	void HandleMessages();
	bool BlockDamage(CrewMember *crew);
	bool BlockAIWeapons();
	bool ResistDamage(ShipManager *ship);
	void PushCrewAIMessage(CrewAI *ai, CrewActionLog &&message);
	void PushCrewReassignedMessage(CrewAI *ai, CrewReassignmentLog &&message);
	float GetGameSpeed(bool tabPressed);

	bool TryHandleWindow(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *result);

	HWND CreateLabel(const std::wstring &label, int x, int y);
	void UpdateTabContents();

	void OpenBlueprintDialogue();

	HFONT generalFont = nullptr;
	bool isInGame = false;
	ShipManager *enemyShipManager = nullptr;

	static void ShowPopupMenu(HMENU menu, HWND parent);
	static std::wstring GetText(HWND hwnd);
	static void SetText(HWND hwnd, const std::wstring &string);

  private:
	void PopulateWindow();
	void PopulateTabAI(bool isEnemyShip, std::unique_ptr<ShipCrewTab> &tab);
	void UpdateLayout();
	void PopulateOverview();
	void UpdateTabItems();
	static void HandleMessagesFor(HWND window);

	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT WindowProcImpl(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	// These are needed for handling messages from buttons on the tab content window
	static LRESULT CALLBACK WindowProcTab(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT WindowProcImplTab(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	bool uiTest = false;
	HINSTANCE instance = nullptr;
	HWND mainWindow = nullptr;
	HWND tabControl = nullptr;
	HWND currentTab = nullptr;

	WNDPROC baseTabWFN = nullptr;

	std::unique_ptr<OverviewTab> overview;
	std::unique_ptr<ShipCrewTab> playerShip;
	std::unique_ptr<ShipCrewTab> enemyShip;
};
